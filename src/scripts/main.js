(function($) {
	window.App = {};
	$(document).ready(function() {

		window.App.Modules = {
			header: require('./inc/header'),
			config: require('./inc/config'),
			menuHamburguer: require('./inc/components/menu-hamburguer'),
			languages: require('./inc/components/languages'),
			user: require('./inc/components/user'),
			map: require('./inc/components/mapbox'),
			search: require('./inc/components/search'),
			favorite: require('./inc/components/favorite'),
			newsletter: require('./inc/components/newsletter'),
			share: require('./inc/components/share'),
			addFavorite: require('./inc/components/add-favorite'),
			weather: require('./inc/components/weather'),
			comboBox: require('./inc/components/combo-box'),
			regionMap: require('./inc/components/region-map'),
			alertGlobal: require('./inc/components/alert-global'),
			destaque: require('./inc/components/destaque'),
			alerts: require('./inc/components/alerts'),
			searchList: require('./inc/components/search-list'),
			cardsPagination: require('./inc/components/cards-pagination'),
			form: require('./inc/components/contact-form'),
			basicTooltip: require('./inc/components/basic-tooltip'),
			cardTooltip: require('./inc/components/card-tooltip'),
			countDown: require('./inc/components/count-down'),
			gallery: require('./inc/components/gallery'),
			customInputDate: require('./inc/components/custom-input-date'),
			popularPosts: require('./inc/components/popular-posts'),
			experienciaNav: require('./inc/components/experiencia-nav'),
			contextualNav: require('./inc/components/contextual-nav'),
			breadcrumb: require('./inc/components/breadcrumb'),
			relatedLinks: require('./inc/components/related-links'),
			comboChangeContent: require('./inc/components/combo-change-content'),
			contactPress: require('./inc/components/contact-press'),
			pressTabs: require('./inc/components/press-tabs'),
			miceContact: require('./inc/components/mice-contact'),
			miceForgotPassword: require('./inc/components/mice-forgot-password'),
			boxMsgError: require('./inc/components/box-msg-error'),
			tradeRegister: require('./inc/components/trade-register'),
			tradeRegister: require('./inc/components/accordeon'),
			currencyConverter: require('./inc/components/currency-converter')
		};

		// Inicializa os modulos que forem inicializaveis
		for(var module in App.Modules) {
			if(typeof App.Modules[module].init == "function") {
				App.Modules[module].init();
			}
		}

	});
})(jQuery)
