"use strict";

function breakpoints(){

	var _public = {};


	// No resize do browser, ele verifica a largura
	// e se for menor que o valor estipulado, ele executa
	// o que for retornado no callback.
	_public.resizeWindow = function resizeWindow(breakpoint, callback1, callback2){

		var timeDebounce = 500;

		jQuery(window).bind('resize', function() {
			var windowWidth = jQuery(window).width();

			if(windowWidth < breakpoint) {
				callback1();
			}
			else {
				callback2();
			}
		}.debounce(timeDebounce));

	};


	// Verifica a largura da janela do browser e
	// executa o que for retornado no callback.
	_public.initialWindow = function initialWindow(breakpoint, callback1, callback2) {
		var windowWidth = jQuery(window).width();

		if(windowWidth < breakpoint) {
			callback1();
		}
		else {
			callback2();
		}
	};


	return _public;

}

module.exports = breakpoints();
