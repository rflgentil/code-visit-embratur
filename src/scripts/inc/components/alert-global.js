var $alertGlobal = $('[data-component="alert-global"]');

function init() {
	handleEventsControl();
}

function handleEventsControl() {

	var $openAlert = $alertGlobal.find('.alert-global__bt-open');
	var $closeAlert = $alertGlobal.find('.alert-global__bt-dismiss');

	$openAlert.on('click', function(e){
		var $alertView = handleAlertView();
		$alertView.open($(this));

		e.preventDefault();
	});

	$closeAlert.on('click', function(e){
		var $alertView = handleAlertView();
		$alertView.close($(this));

		e.preventDefault();
	});
}

function handleAlertView() {

	var open = function(el) {
		el.fadeOut().promise().done(function(){
			el.parent().find('.alert-global__bg-wrapper').fadeIn();
		});
	}

	var close = function(el) {
		el.parents('.alert-global__bg-wrapper').fadeOut();
		el.parents('.alert-global').find('.alert-global__bt-open').fadeOut().remove();
	}

	return {
		open: open,
		close: close
	}
}

module.exports = {
	init: init
}
