var $relatedLinksFixed = $('.related-links--fixed');
var $breakpoints = require('../helpers/breakpoint');

function init() {
	if($relatedLinksFixed.length >= 1) {

		// No carregamento da página.
		$breakpoints.initialWindow(1208, function() {
			$relatedLinksFixed.removeClass("active");
		}, function(){
			fixedBox()
		});

		// No resize do browser.
		$breakpoints.resizeWindow(1208, function() {
			$relatedLinksFixed.removeClass("active");
		}, function(){
			fixedBox();
		});

	}
}

function fixedBox() {
	$(window).scroll(function () {
		if($(window).width() < 1208) return false;

		if ($(this).scrollTop() > 850) {
		  $relatedLinksFixed.addClass("active");
		} else {
		  $relatedLinksFixed.removeClass("active");
		}
	});
}

module.exports = {
	init: init
}
