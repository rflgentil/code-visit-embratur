var $comboChangeContent = $('.custom-combo--change-content');

function init() {
	if($comboChangeContent.length >= 1) changeContent();
}

function changeContent() {
	$comboChangeContent.find('.custom-combo__options-item').on('click', function(){
		var eleTarget = $(this).data('target');

		$('.combo-change-content').fadeOut(500);
		$('#' + eleTarget).fadeIn(500);
	});
}

module.exports = {
	init: init
}
