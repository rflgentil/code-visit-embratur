"use strict";

/**
 * GLOBAL.loadGoogleMapsAPIInitialized
 * GLOBAL.apiLoaded
 *
 * Links importantes
 * @link https://developers.google.com/maps/documentation/javascript/examples/overlay-symbol-custom?hl=pt-br
 * @link https://developers.google.com/maps/documentation/javascript/examples/overlay-symbol-dashed?hl=pt-br
 * @link https://developers.google.com/maps/documentation/javascript/examples/icon-complex?hl=pt-br
 *
 * Encontrando o midpoint de duas coordenadas para centralizar o mapa
 * @link https://www.quora.com/How-do-I-calculate-the-midpoint-between-two-points-on-Google-Maps/answer/Thor-Mitchell?srid=936Z
 *
 * Marker com HTML customizado
 * @link http://humaan.com/custom-html-markers-google-maps/
 */
module.exports = function($el) {
	window._embratur_component_map = window._embratur_component_map ? window._embratur_component_map : {};
	var self = this;
	var CustomMarker = null;
	var CustomMarkerPointLabel = null;
	var CustomMarkerPointNumber = null;
	var GLOBAL = window._embratur_component_map;
	var MAP = null;
	var $EL = $el;
	var UID = parseInt(Math.random()*1000000000);
	var EXEC_QUEUE = [];


	/**
	 * Constructor do objeto
	 */
	function __construct() {
		// Se a API já tiver sido carregada, inicializa direto o mapa
		if(GLOBAL.apiLoaded && GLOBAL.apiLoaded===true) init();

		// Se não inicializou direto o mapa, verifica se existe a fila de
		// carregamento (se não, cria) e já adiciona o inicializador na fila.
		if(!GLOBAL.initQueue) GLOBAL.initQueue = [];
		GLOBAL.initQueue.push(init);

		// Debounce para chamar o load da API
		clearTimeout(GLOBAL._loadGoogleMapsAPI);
		GLOBAL._loadGoogleMapsAPI = setTimeout(loadGoogleMapsAPI, 100)
	}


	/**
	 * Carrega, apenas uma vez, a API do google maps
	 * O contexto desta função é o último objeto gerado de Maps.
	 */
	function loadGoogleMapsAPI() {
		// Verifica se já foi iniciada alguma chamada à API
		if(GLOBAL.loadGoogleMapsAPIInitialized && GLOBAL.loadGoogleMapsAPIInitialized===true) return false;
		GLOBAL.loadGoogleMapsAPIInitialized = true;

		// Cria o callback do load do Google maps
		GLOBAL.fnCallbackLoadAPI = function(data) {
			// Seta a API como carregada
			GLOBAL.apiLoaded = true;

			// Inicializa os mapas que estão na fila
			for(var x=0; x<GLOBAL.initQueue.length; x++) {
				GLOBAL.initQueue[x]();
			}
		}

		// Carrega a API
		$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBgzWu63wdSVYCmTv7eHFcFuzwUwN67O0c&callback=_embratur_component_map.fnCallbackLoadAPI&libraries=geometry');
	}


	/**
	 * Inicializa o mapas
	 */
	function init() {
		var center = new google.maps.LatLng(-14.235004, -51.92528);

		MAP = new google.maps.Map($EL[0], {
			scrollwheel: false,
			zoom: 2,
			maxZoom: 17,
			center: center
		});

		// Seta o CustomMarker
		CustomMarker = require('./map/CustomMarker');
		CustomMarkerPointLabel = require('./map/CustomMarker--PointLabel');
		CustomMarkerPointNumber = require('./map/CustomMarker--PointNumber');

		// Executa as funções que ficaram na fila
		execQueuedFunctions();
	}


	/**
	 * Traça linha pontilhada entre 2 pontos
	 * @param a {ARRAY}: Lat e Lng do ponto A: [lat, lng]
	 * @param b {ARRAY}: Lat e Lng do ponto B: [lat, lng]
	 * @param options {OBJECT}:
	 *	addMarkers {BOOL}: Adiciona os markers no começo e fim da linha. Default: true
	 *	tempoDeVoo {INT}: Tempo de voo que será colocado no label
	 *
	 * Ex.: addLinha([51.507351, -0.127758], [-23.550520, -46.633309])
	 */
	function addLine(a, b, options) {
		if(execOrQueue('addLine', arguments)) return false;

		var settings = {
			addMarkers: true,
		};
		jQuery.extend(settings, options);

		// Symbol com a linha pontilhada
		var lineSymbol = { path: 'M 0,-1 0,1', strokeOpacity: 1, scale: 1.2 };

		// Seta os pontos A e B e encontra o midpoint
		var pointA = new google.maps.LatLng(a[0], a[1]);
		var pointB = new google.maps.LatLng(b[0], b[1]);
		var midpoint = google.maps.geometry.spherical.interpolate(pointA, pointB, 0.5);
		var heading = google.maps.geometry.spherical.computeHeading(pointA, pointB);

		// Adiciona Symbol
		var symbolCircle = {
			path: google.maps.SymbolPath.CIRCLE,
			fillColor: '#fff',
			fillOpacity: 1,
			strokeOpacity: 1,
			strokeColor: '#7fc23f',
			strokeWeight: 1.5,
			scale: 19
		}

		var symbolAirplane = {
			path: 'M 19.256 37.224 C 18.232 37.097 17.337 36.458 16.698 35.05 C 16.058 33.645 15.675 32.236 15.675 30.574 L 15.675 25.458 L 2.118 17.017 C 1.863 16.889 1.734 16.634 1.734 16.25 L 2.246 12.669 C 2.246 12.54 2.373 12.413 2.502 12.285 C 2.502 12.285 2.63 12.157 2.63 12.157 C 2.758 12.03 3.013 12.03 3.269 12.03 L 15.803 15.61 L 15.803 7.425 L 11.838 5.25 C 11.582 5.123 11.454 4.867 11.454 4.611 L 11.454 1.542 C 11.454 1.413 11.582 1.158 11.711 1.03 L 11.711 1.03 C 11.838 0.903 12.094 0.775 12.35 0.903 L 19.384 1.925 L 26.418 0.903 C 26.546 0.903 26.802 0.903 26.93 1.03 C 27.058 1.158 27.185 1.286 27.185 1.542 L 27.185 4.611 C 27.185 4.74 27.058 4.995 26.93 5.123 C 26.93 5.123 26.802 5.25 26.802 5.25 L 22.836 7.553 L 22.836 15.738 L 35.371 12.157 C 35.626 12.157 35.754 12.157 36.01 12.285 C 36.138 12.413 36.266 12.669 36.266 12.797 L 36.266 16.889 C 36.266 17.146 36.138 17.401 35.882 17.529 L 22.836 25.586 L 22.836 30.702 C 22.836 32.365 22.453 33.771 21.814 35.179 C 21.047 36.458 20.279 37.097 19.256 37.224 L 19.256 37.224 Z',
			fillColor: '#5c5c5c',
			fillOpacity: 1,
			rotation: 180,
			strokeOpacity: 0,
			scale: 0.7,
			anchor: new google.maps.Point(19, 19),
			strokeWeight: 0,
		};

		// Adiciona tempo da viagem
		var overlay = new CustomMarker(pointA, pointB, MAP, {
			heading: heading,
			tempoDeVoo: settings.tempoDeVoo
		});

		// Adiciona a linha ao mapa
		var line = new google.maps.Polyline({
			path: [pointA, pointB],
			strokeOpacity: 0,
			geodesic: false,
			icons: [
				{
					icon: lineSymbol,
					offset: '0',
					repeat: '7px'
				},
				{
					icon: symbolAirplane,
					offset: '50%'
				}
			],
			map: MAP
		});

		if(settings.addMarkers) {
			addMarker(a);
			addMarker(b);
		}
	}


	/**
	 * Adiciona o marcador verde em um determinado ponto
	 * @param point {ARRAY|LatLngObject}: Lat e Lng do ponto A: [lat, lng]
	 * @param options {OBJECT}: Opções de configuração do marker:
	 * 			imageUrl {STRING}: URL da imagem do marker
	 *			imageSize {ARRAY}: Tamanho natural da imagem [w, h]
	 *			imageAnchor {ARRAY}: Ponto da imagem que vai ser usado para pinar [x, y]
	 */
	function addMarker(point, options) {
		if(execOrQueue('addMarker', arguments)) return false;

		var settings = {
			imageUrl: '/images/map-marker--green.svg',
			imageSize: [16, 16],
			imageAnchor: [8, 8]
		};
		jQuery.extend(settings, options);

		// Se o point é array, converte para objeto
		if(point.constructor === Array) point = {lat: point[0], lng: point[1]}

		var markerImage = {
			url: settings.imageUrl,
			size: new google.maps.Size(settings.imageSize[0], settings.imageSize[1]),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(settings.imageAnchor[0], settings.imageAnchor[1]),
			rotation: 110
		}

		var marker = new google.maps.Marker({
			position: point,
			map: MAP,
			icon: markerImage,
		});
	}


	/**
	 * Adiciona um balão com label em determinado ponto
	 * @param point {ARRAY|LatLngObject}: Lat e Lng do ponto A: [lat, lng]
	 * @param label {STRING}: Label do ponto
	 * @param bold {BOOL}: Se o texto é em destaque ou não
	 */
	function addPointLabel(point, label, bold) {
		if(execOrQueue('addPointLabel', arguments)) return false;

		// Se o point é array, converte para objeto
		if(point.constructor === Array) point = {lat: point[0], lng: point[1]}
		point = new google.maps.LatLng(point.lat, point.lng);

		// Adiciona tempo da viagem
		var overlay = new CustomMarkerPointLabel(point, MAP, {
			label: label,
			bold: bold
		});
	}


	/**
	 * Adiciona um pin com número dentro
	 * @param point {ARRAY|LatLngObject}: Lat e Lng do ponto A: [lat, lng]
	 * @param number {INT}: Número de dentro do PIN
	 * @param verdePadrao {BOOL}: Se true, mostra o verde padrão. False, o escuro
	 */
	function addPointNumber(point, number, verdePadrao) {
		if(execOrQueue('addPointNumber', arguments)) return false;

		// Se o point é array, converte para objeto
		if(point.constructor === Array) point = {lat: point[0], lng: point[1]}
		point = new google.maps.LatLng(point.lat, point.lng);

		// Adiciona tempo da viagem
		var overlay = new CustomMarkerPointNumber(point, MAP, {
			number: number,
			verdePadrao: verdePadrao
		});
	}


	/**
	 * Centraliza o mapa em uma cordenada ou entre 2 coordenadas
	 * @param pointA {ARRAY}: Lat e Lng do ponto A: [lat, lng]
	 * @param pointB {ARRAY}: Lat e Lng do ponto B: [lat, lng] (opt)
	 */
	function centerMap(pointA, pointB) {
		if(execOrQueue('centerMap', arguments)) return false;

		if(pointA && pointB) {
			// Centraliza entre 2 pontos
			pointA = new google.maps.LatLng(pointA[0], pointA[1]);
			pointB = new google.maps.LatLng(pointB[0], pointB[1]);
			var midpoint = google.maps.geometry.spherical.interpolate(pointA, pointB, 0.5);
			MAP.setCenter(midpoint);
		} else {
			// Centraliza no ponto
			pointA = new google.maps.LatLng(pointA[0], pointA[1]);
			MAP.setCenter(pointA);
		}
	}


	/**
	 * Checa se A API já foi carregada antes de executar a função.
	 * Se já foi carregada, retorna false para que a função possa executar
	 * normalmente. Se ainda não foi carregada, adiciona a uma fila de execução
	 * que vai ser executada no init()
	 *
	 * @param fn {STRING}: Nome da função que deverá ser executada
	 * @param args {OBJECT}: Objeto arguments da função
	 *
	 * @return {BOOL}: TRUE se precisou ser adicionada na fila, FALSE, se não.
	 */
	function execOrQueue(fn, args) {
		if(GLOBAL.apiLoaded && GLOBAL.apiLoaded===true) return false;
		var keys = [];

		keys.push(fn);
		for(var x=0; x<args.length; x++) {
			keys.push(args[x]);
		}

		EXEC_QUEUE.push(keys);

		return true;
	}


	/**
	 * Executa toda fila de funções acumulada antes do load da API
	 */
	function execQueuedFunctions() {
		for(var x=0; x<EXEC_QUEUE.length; x++) {
			var fnName = EXEC_QUEUE[x].shift();
			self.API[fnName].apply(this, EXEC_QUEUE[x]);
		}
	}


	// Chama o constructor
	__construct();


	// API publica
	return self.API = {
		addLine: addLine,
		addMarker: addMarker,
		addPointLabel: addPointLabel,
		addPointNumber: addPointNumber,
		centerMap: centerMap
	}
}
