var $boxMsgError = $('[data-component="box-msg-error"]');

function init() {
	if($boxMsgError.length >= 1) handleCloseBoxError();
}

function handleCloseBoxError() {
	var $btClose = $boxMsgError.find('.box-msg-error__btn-close-error');

	$btClose.on('click', function(e) {
		e.preventDefault();

		$(this).parent().slideUp(500);
	});
}


module.exports = {
	init: init
}
