var $weatherComponent = $('[data-component=weather]');
var $weatherScaleComponent = $('[data-component=weather-scale]');

function init() {
	if($weatherComponent.length >= 1) {
		handleScale();
		handleWeatherSmall();
	}
}

function handleScale() {
	var $key = $weatherScaleComponent.find('.weather__scale-switch-text');

	$key.on('click', function(e) {
		e.preventDefault();

		if( !$(this).hasClass('active') ) {
			$(this).addClass('active').siblings().removeClass('active');
		}
	});
}

function handleWeatherSmall() {

	if($('.weather--small').length < 1) return;

	var $weatherButton = $('.weather--small__button');
	var $wrapper = $('.weather__wrapper');

	$weatherButton.on('click', function() {
		$(this).next().toggleClass('active');
		$(this).find('.weather--small__button-inside').toggleClass('active');
	});


}

module.exports = {
	init: init
}
