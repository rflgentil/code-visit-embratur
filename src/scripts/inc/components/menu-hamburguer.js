var $MenuHamburguerComponent = $('[data-component=menu-hamburguer]');

function init() {
	toggleMenu();
}

function toggleMenu() {
	$MenuHamburguerComponent.on('click', function() {
		var $this = $(this);

		$this.toggleClass('is-active');
		$('.header__bottom-area').toggleClass('header__bottom-area--nav-active');
	});
}

module.exports = {
	init: init
}
