var $pressTabsComponent = $('[data-component=press-tabs]'),
	$tabItem 			= $('.imprensa__tabs-item'),
	linkTab 			= $pressTabsComponent.find('.imprensa__tabs-link');

function init() {
	if($pressTabsComponent.length >= 1) {
		handleItem();
		showFilterResults();
		descriptionGuides();

		$breakpoints.initialWindow(768, function() {
			autoHeight();
		}, function() {
			adaptedHeight();
		});

		$breakpoints.resizeWindow(768, function() {
			autoHeight();
		}, function() {
			adaptedHeight();
		});


	}
}



function handleItem(changeEvent) {
	linkTab.on('click', function(e) {
		e.preventDefault();

		var $this 			= $(this),
			linkTarget 		= $this.attr('href'),
			prevItemActive 	= $('.imprensa__tabs-item--active').find('.imprensa__tabs-link').attr('href').split('#'),
			listClass 		= $('.imprensa__results-wrapper').attr('class').replace(prevItemActive[1], linkTarget.replace('#', '')),
			equalItem 		= $this.parent().hasClass('imprensa__tabs-item--active'),
			windowWidth 	= $(window).width();


		if(equalItem || linkTarget === '#press-filter-results' || linkTarget === '#trade-filter-results') return;

		$pressTabsComponent.removeClass('imprensa__tabs--filter-results');

		// Troca classe modificadora para a classe da área selecionada.
		$('.imprensa__results-wrapper').attr('class', listClass);

		// Ativa item do menu
		$tabItem.removeClass('imprensa__tabs-item--active');
		$this.parent().addClass('imprensa__tabs-item--active');

		if(windowWidth >= 768) {
			adaptedHeight();

			$('.imprensa__results').fadeOut(500);
			$(linkTarget).fadeIn(500);
		}
		else {
			$('.imprensa__results').slideUp(500);
			$(linkTarget).slideDown(500, function() {
				var itemPosition = $('.imprensa__tabs-item--active').offset();

				$('html, body').stop(true, true).animate({scrollTop: itemPosition.top}, 1000);
			});
		}
	});
}


function adaptedHeight() {
	autoHeight();
	var blockHeight = $('.imprensa__tabs-item--active').find('.imprensa__results').outerHeight();

	$('.imprensa__results-wrapper').height(blockHeight);
	$('.imprensa__results').height(blockHeight -32);
}


function autoHeight() {
	$('.imprensa__results-wrapper, .imprensa__results').height('auto');
}


function descriptionGuides() {
	$('.imprensa__description-guides-close').on('click', function(event) {
		event.preventDefault();

		$(this).parents('.imprensa__description-guides').slideUp(500, function() {
			adaptedHeight();
		});
	});
}


// Remover quando for aplicar filtros e paginação.
// Serve somente para mostrar a página estática p/ o QA
// validar antes da integração.
function showFilterResults() {
	if(!$('.imprensa__tabs-item--filter-results').hasClass('imprensa__tabs-item--active')) return;

	$pressTabsComponent.addClass('imprensa__tabs--filter-results');
	$('.imprensa__results--press-release').css('display', 'none');
	$('.imprensa__results--press-filter-results, .imprensa__results--trade-filter-results').css('display', 'block');
}



module.exports = {
	init: init
}
