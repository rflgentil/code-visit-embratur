var $newsletterComponent = $('[data-component=newsletter]');

function init() {
	submitNewsletter();
}

function submitNewsletter() {
	$('.btnNewsLetter').click(function(){

		$('#newsletter-form').submit(function () { return false; });

		var sEmail	= $('.newsletterMail').val();
		var emailFilter=/^.+@.+\..{2,}$/;
		var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/

		if(!(emailFilter.test(sEmail))||sEmail.match(illegalChars)){
			$('.fail').show();
			$('.newsletterMail').css("border-color", "red");
		}else{

			$.ajax({
				url:'/',
				type: 'GET',
				data: {email: sEmail},
				success: function(){
					$('.fail').hide();
					$('.newsletterMail').css("border-color", "transparent");
					$('#newsletter-form').fadeOut(250);
					$('.sucesso').fadeIn(1400);
					window.setTimeout(function (argument) {
						$('.sucesso').fadeOut(350);
					},4000)
					window.setTimeout(function (argument) {
						$('#newsletter-form').fadeIn(2400);
						$('.newsletterMail').val('');
					},4500)
				}
			});
		}
	})
}

module.exports = {
	init: init
}
