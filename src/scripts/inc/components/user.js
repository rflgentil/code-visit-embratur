var $userComponent = $('[data-component=user]');

function init() {
	toggleUser();
}

function toggleUser() {
	$userComponent.find('.user__button').on('click', function() {
		var $this = $(this);

		$this.parent('[data-component=user]').toggleClass('user--open');
	});

	$userComponent.find('.user__list-item-link').on('click', function() {
		var $this = $(this);

		$userComponent.removeClass('user--open');
	});
}

module.exports = {
	init: init
}
