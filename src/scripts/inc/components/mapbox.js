$breakpoints = require('../helpers/breakpoint.js');
var $map 	= $('[data-component="map"]'),
	map;

function init() {
	if($map.length >= 1) loadMap();
}

function loadMap() {
	var page = $('[data-component="map"]').data('page');
	$map = $('[data-component="map"]')

	L.mapbox.accessToken = 'pk.eyJ1IjoiaXNvYmFyIiwiYSI6ImNpbHk2anpucDA4Mmx1c2tzNG5iaHA0c3IifQ.hG69Z0BUxbk2sFfcgxCySA';

	requestService(page);
}


function addMarker(data) {

	// Percorre os mapas da página faz o load do mapa e adiciona os pins.
	$map.each(function(i) {
		var $this 		= $(this),
			map 		= $this.attr('id'),
			iconDefault	= $this.data('type'),
			iconClass 	= '',
			iconTotal 	= '';

		map = L.mapbox.map(map, 'mapbox.streets');

		// Desabilita zoom do mapa no scroll e teclado.
		map.scrollWheelZoom.disable();
		map.keyboard.disable();

		// Seta latitude, longitude e zoom do mapa.
		map.setView([data[i].lat, data[i].lon], data[i].zoom);

		if(data[i].marker !== undefined) {
			data[i].marker.forEach(function (item, index) {

				iconClass = item.total !== undefined || iconDefault === undefined ? setTypeClass(item) : iconDefault;
				iconTotal = item.total === undefined ? '' : item.total;

				// Cofigurações do icone
				var cssIcon = L.divIcon({
					className: iconClass,
					html: iconTotal
				});

				// Seta latitude, longitude e configurações do ícone.
				L.marker([item.lat, item.lon], {icon: cssIcon}).addTo(map);

			});
		}

		mobileMap(map, data, i);

	});
}


// Retorna o tipo de icone que vai ser exibido no mapa.
function setTypeClass(item) {
	var typeIcon = '';

	if(item.type === 'experience') {
		typeIcon = 'map__star-icon map__star-icon--orange'
	}
	else if(item.type === 'airport') {
		typeIcon = 'map__simple-icon map__simple-icon--blue'
	}
	else if(item.total !== undefined) {
		typeIcon = 'map__number-icon'
	}
	else {
		typeIcon = 'map__simple-icon map__simple-icon--green'
	}

	return typeIcon;
}


// Faz o request para a API retornada do back com os dados do mapa.
function requestService(page) {
	var request = $.ajax({
		url: App.Modules.config.url.map[page],
		method: 'GET',
		dataType: 'json'
	});

	request.done(function(data) {
		addMarker(data);
	});

	request.fail(function(jqXHR, textStatus) {
		console.log('Request Error!');
	});
}


// Modifica o zomm do mapa na versão mobile
function mobileMap(map, data, i) {
	$breakpoints.resizeWindow(767, function() {
		map.setView([data[i].lat, data[i].lon], data[i].zoom - 2);
	}, function() {
		map.setView([data[i].lat, data[i].lon], data[i].zoom);
	});

	$breakpoints.initialWindow(767, function() {
		map.setView([data[i].lat, data[i].lon], data[i].zoom - 2);
	}, function() {
		map.setView([data[i].lat, data[i].lon], data[i].zoom);
	});
}


module.exports = {
	init: init,
	loadMap: loadMap
}
