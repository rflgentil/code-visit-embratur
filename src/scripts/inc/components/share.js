var $shareComponent = $('[data-component=share]'),
	$twitterButton 	= $('.share__item-social--twitter'),
	$facebookButton = $('.share__item-social--facebook'),
	$gogleButton 	= $('.share__item-social--plus');


function init() {
	if($shareComponent.length >= 1) {
		handleTwitterSharing();
		handleFacebookSharing();
		handleGoogleSharing();
	}
}



function handleTwitterSharing() {
	$twitterButton.on('click', function(e) {
		e.preventDefault();

		var textShare = $(this).attr('data-textShare');

		openPopup('https://twitter.com/share?text=' + textShare, 600, 300);
	})
}


function handleFacebookSharing() {
	$facebookButton.on('click', function(e) {
		e.preventDefault();

		openPopup('http://www.facebook.com/sharer.php?u='+window.location.href, 600, 300);
	})
}


function handleGoogleSharing() {
	$gogleButton.on('click', function(e) {
		e.preventDefault();

		openPopup('https://plus.google.com/share?url='+window.location.href, 600, 300);
	})
}


function openPopup(url, popWidth, popHeight) {

	var settings = {
		screenY: (window.screen.height / 2) - ((popHeight / 2) + 50),
		screenX: (window.screen.width / 2) - ((popWidth / 2) + 10),
		height: popHeight,
		width: popWidth
	}

	window.open(
		url,
		'',
		"status=no," +
		"height="+ settings.height + ',' +
		"width="+ settings.width + ',' +
		"screenX="+ settings.screenX + ',' +
		"screenY="+ settings.screenY + ',' +
		",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no"
	)
}



module.exports = {
	init: init
}

