var $experienciaNav = $('[data-components="experiencia-nav"]');

function init() {
	if( $experienciaNav.length > 0 ) {
		handleNavClick();
		handleNavClickMobile();
		handleNavFix();
		handleScrollBetweenDays();
	}
}

function handleNavClick() {

	$experienciaNav.find('.experiencia-single__nav-day').on('click', function(e) {
		e.preventDefault();

		$experienciaNav.find('.experiencia-single__nav-day').removeClass('active');

		$(this).addClass('active');
		handleScroll( $($(this).attr('href')), -165 );
	});
}

function handleNavClickMobile() {
	var $listWrapper = $('.experiencia-single__nav-days-wrapper');
	$experienciaNav.find('.experiencia-single__nav-days-label').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$listWrapper.toggleClass('active');


	});
}

function handleScroll(target, sub) {
	var scroll = sub ? target.offset().top + sub : target.offset().top;

	$('html, body').animate({
	  scrollTop: scroll
	}, 1000);

	return false;
}

function handleNavFix() {

	var $breakpoints = require('../helpers/breakpoint');
	var $containerDestaque = $('.destaque');
	var top = $containerDestaque.height() + $('.experiencia-single__post').height();

	$breakpoints.resizeWindow(1200, function() {
		top = $containerDestaque.height() + $('.experiencia-single__post').height();
	}, function(){
		top = $containerDestaque.height() + $('.experiencia-single__post').height();
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() > top) {
		  $experienciaNav.addClass("experiencia-single__nav--fixed");
		} else {
		  $experienciaNav.removeClass("experiencia-single__nav--fixed");
		}
	});
}

function handleScrollBetweenDays() {

	var num = 0;
	var $headerFix = $('.experiencia-single__nav');
	var $linkNav = $('.experiencia-single__nav-day');

	$(window).on('scroll', function() {

		var $scrollOffset = $(window).scrollTop() + $headerFix.height();
		var $i = 0;

			$('[data-day]').each(function() {
				if( $scrollOffset >= $(this).offset().top) { $i++; }
			});

			if(num != $i || $i == 0) {
				num = $i;

				$linkNav.removeClass('active');
				$('[data-ref="'+num+'"]').addClass('active');
			}
	});
}
module.exports = {
	init: init
}
