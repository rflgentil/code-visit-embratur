var $galleryComponent 	= $('[data-component=gallery]'),
	$openGallery 		= $('[data-gallery]'),
	$btnChangeMode 		= $('.gallery__change-mode'),
	$btnClose 			= $('.gallery__close-btn'),
	$thumbImage			= $('.gallery__thumb-link'),
	$btnNext 			= $('.gallery__btn-nav--right'),
	$btnPrev 			= $('.gallery__btn-nav--left'),
	$bigImage 			= $('.gallery__wrap-full-image img'),
	$wrapDetailsImage	= $('.gallery__wrap-details-image'),
	dataImage 			= {};


function init() {
	if($galleryComponent.length >= 1) {
		startEvents();
	}
}



function startEvents() {
	$btnChangeMode.on('click', changeGalleryMode);
	$thumbImage.on('click', openImageDetails);
	$btnNext.on('click', changeImages);
	$btnPrev.on('click', changeImages);
	$openGallery.on('click', openGallery);
	$btnClose.on('click', closeGallery);
	$bigImage.on('click', openImageFull);
	$(document).on('click', '.gallery__close-btn--fullscreen', closeImageFull);
}


// Fecha Galeria de fotos.
function closeGallery() {
	var $this = $(this);

	$this.parents('[data-component=gallery]').fadeOut(500);
}


// Abre galeria de fotos.
function openGallery(event) {
	event.preventDefault();

	var $this 		= $(this),
		idGallery 	= $this.data('gallery');

	$('#' + idGallery).fadeIn(500);
}


// Abre imagem fullscreen no mobile.
function openImageFull() {
	var $this 			= $(this),
		isMobile 		= $(window).width() <= 767,
		templateMask 	= {
			'html': '<div class="gallery__mask-full-screen">' +
						'<button class="gallery__to-zoom">To Zoom</button>' +
						'<button class="gallery__close-btn gallery__close-btn--white gallery__close-btn--fullscreen"></button>' +
					'</div>'
		}


	if(isMobile) {
		$this.parents('[data-component=gallery]').addClass('gallery--fullscreen').append(templateMask.html);
		$wrapDetailsImage.addClass('hidden');
	}
}

// Fecha imagem fullscreen no mobile.
function closeImageFull() {
	var $this = $(this);

		$this.parents('[data-component=gallery]').removeClass('gallery--fullscreen')
		.find('.gallery__mask-full-screen').fadeOut(500, function() {
			$(this).remove();
			$wrapDetailsImage.removeClass('hidden');
		})
}


// Troca o modo da galeria, de thumbs para detalhes da foto.
function changeGalleryMode(event) {
	var $this = $(this);

	if($this.hasClass('gallery__change-mode')) {
		event.preventDefault();

		openImageDetails($this);
	}
	else {
		$this = event;
	}

	$this.parents('[data-component=gallery]').find('.gallery__thumbs').toggleClass('gallery__thumbs--hidden');
	$this.parents('[data-component=gallery]').find('.gallery__details-image').toggleClass('gallery__details-image--active');
}


// Abre detalhes da imagem ativa.
function openImageDetails(event) {
	var $this = $(this);

	// Entre somente se o click for no thumb da imagem.
	if($this.hasClass('gallery__thumb-link')) {
		event.preventDefault();

		$this.parents('.gallery__list-thumbs').find('.gallery__thumb-link').removeClass('active');
		$this.addClass('active');
	}
	else {
		$this = event;
	}

	changeDetailsImage($this);

	// Faz a troca somente se o click for no thumb da imagem.
	if($this.hasClass('gallery__thumb-link')) changeGalleryMode($this);
}


// Popula template de detalhes da imagem.
function changeDetailsImage($this) {
	var $activeGallery 	= $this.parents('[data-component=gallery]'),
		$activeImage 	= $activeGallery.find('.gallery__thumb-link.active');

	dataImage.fullImage 		= $activeImage.data('image');
	dataImage.nameImage 		= $activeImage.data('name');
	dataImage.descriptionImage 	= $activeImage.data('description');
	dataImage.downloadImage 	= $activeImage.data('download');
	dataImage.sizeImage 		= $activeImage.data('size');
	dataImage.activeImage 		= $activeImage.parent('.gallery__thumbs-item').index();
	dataImage.totalImage 		= $activeGallery.find('.gallery__thumbs-item').length;

	$activeGallery.find('.gallery__wrap-full-image img').attr({src: dataImage.fullImage, alt: dataImage.nameImage});
	$activeGallery.find('.gallery__name-image').text(dataImage.nameImage);
	$activeGallery.find('.gallery__description').text(dataImage.descriptionImage);
	$activeGallery.find('.gallery__details-image .btn-download').attr('href', dataImage.downloadImage);
	$activeGallery.find('.gallery__details-image .btn-download__size').text('(' + dataImage.sizeImage + ')');
	$activeGallery.find('.gallery__active-image').text(dataImage.activeImage + 1);
	$activeGallery.find('.gallery__total-image').text(dataImage.totalImage);
}


// Navegação das fotos.
function changeImages() {
	var $this = $(this),
		typeButton = $this.hasClass('gallery__btn-nav--right'),
		activeImage = $this.parents('[data-component=gallery]').find('.gallery__thumb-link.active').parent('.gallery__thumbs-item').index(),
		totalImages = $this.parents('[data-component=gallery]').find('.gallery__thumb-link').length,
		indexImage = typeButton ? activeImage + 1 : activeImage - 1;


	if(indexImage > totalImages - 1) {
		indexImage = 0;
	}
	else if(indexImage <= 0) {
		indexImage = totalImages - 1;
	}

	$this.parents('[data-component=gallery]').find('.gallery__thumb-link.active').removeClass('active');
	$this.parents('[data-component=gallery]').find('.gallery__thumbs-item').eq(indexImage).children('.gallery__thumb-link').addClass('active');

	changeDetailsImage($this);
}



module.exports = {
	init: init,
	dataImage: dataImage
}

