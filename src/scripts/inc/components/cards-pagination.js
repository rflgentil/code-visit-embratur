var $cardsCarouselBullets = $('[data-cards-carousel--bullets]')
var $cardsCarouselPagination = $('[data-cards-carousel--pagination]')

function init() {
	var mobileBrekpoint = 767;

	if ( $cardsCarouselBullets.length > 0 ) {
		$breakpoints = require('../helpers/breakpoint.js');
		$breakpoints.resizeWindow(mobileBrekpoint, function() {

			handleCarouselBullets();
		}, function() {});

		$breakpoints.initialWindow(mobileBrekpoint, function() {
			handleCarouselBullets();
		}, function() {});

	}

	if ( $cardsCarouselPagination.length > 0 ) {
		$breakpoints = require('../helpers/breakpoint.js');

		$breakpoints.resizeWindow(mobileBrekpoint, function() {
			handleCarouselPagination();
		}, function() {});

		$breakpoints.initialWindow(mobileBrekpoint, function() {
			handleCarouselPagination();
		}, function() {});

	}
}

function handleCarouselBullets() {
	if($cardsCarouselBullets.hasClass('slick-slider')) return;
	$cardsCarouselBullets.slick({
		variableWidth: true,
		mobileFirst: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		centerPadding: '20px',
		lazyLoad: 'ondemand',
		arrows: false,
		dots: true,

		responsive: [{
			breakpoint: 320,
			settings: {
				centerPadding: '40px',
			}
		},{
			breakpoint: 400,
			settings: {
				centerPadding: '80px',
			}
		},
		{
			breakpoint: 530,
			settings: {
				centerPadding: '100px',
			}
		},
		{
			breakpoint: 600,
			settings: {
				centerPadding: '140px',
			}
		},{
			breakpoint: 767,
				settings: 'unslick'
		}]
	});
}


function handleCarouselPagination() {
	// var $bt_prev = $('.pagination__nav--prev');
	// var $bt_next = $('.pagination__nav--next');

	if($cardsCarouselPagination.hasClass('slick-slider')) return;

	$cardsCarouselPagination.each(function() {
		var $this 		= $(this),
			$bt_prev 	= $this.parent().find('.pagination__nav--prev'),
			$bt_next 	= $this.parent().find('.pagination__nav--next'),
			$total 		= $this.find('>div').length;

		$this.parent().find('.pagination__pages-total').html($total);

		$this.slick({
			variableWidth: true,
			mobileFirst: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			centerMode: true,
			centerPadding: '20px',
			lazyLoad: 'ondemand',
			arrows: false,
			dots: false,
			prevArrow: $bt_prev,

			responsive: [{
				breakpoint: 320,
				settings: {
					centerPadding: '40px',
				}
			},{
				breakpoint: 400,
				settings: {
					centerPadding: '80px',
				}
			},
			{
				breakpoint: 530,
				settings: {
					centerPadding: '100px',
				}
			},
			{
				breakpoint: 600,
				settings: {
					centerPadding: '140px',
				}
			},{
				breakpoint: 767,
					settings: 'unslick'
			}]
		});

		$bt_prev.on('click', function () {
			$this.slick('slickPrev')
		});

		$bt_next.on('click', function () {
			$this.slick('slickNext')
		});

		$this.on('afterChange', function(event, slick, currentSlide) {
			$this.parent().find('.pagination__pages-current').html(currentSlide+1)
		});

	});




}

module.exports = {
	init: init
}
