var $basicTooltip = $('[data-component=basic-tooltip]');


function init() {
	if($basicTooltip.length >= 1) startTooltip();
}


function startTooltip() {
	$basicTooltip.on('mouseenter', function(){
		var $this 			= $(this),
			widthEle 		= $this.outerWidth() / 2,
			text 			= $this.attr('data-tooltip'),
			offSet 			= $this.offset(),
			widthTooltip 	= 0;


		if($this.find('.basic-tooltip').length >=1 ) return false;


		$this.append('<div class="basic-tooltip">' + text + '</div>');

		widthTooltip = $this.find('.basic-tooltip').outerWidth() / 2;

		$this.find('.basic-tooltip').css({'top': offSet.top - 25,'left': (offSet.left + widthEle) - widthTooltip})
		.stop(true, true).animate({'opacity': 1, 'top': offSet.top - 35}, 300);
	});

	$basicTooltip.on('mouseleave', function(){
		var $this 	= $(this),
			offSet 	= $this.offset();

		$this.find('.basic-tooltip').stop(true, true).animate({'opacity': 0, 'top': offSet.top - 25}, 300, function(){
			$(this).remove();
		});

	});
}


module.exports = {
	init: init
}
