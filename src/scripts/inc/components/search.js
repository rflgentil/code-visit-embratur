var $searchComponent = $('[data-component=search]');

function init() {
	var $breakpoints = require('../helpers/breakpoint');
	var $searchFilter = handleSearchFilter();

	$breakpoints.resizeWindow(1200, function() {
		$searchFilter.on();
	}, function(){
		$searchFilter.off();
	});

	$breakpoints.initialWindow(1200, function() {
		$searchFilter.on();
	}, function(){
		$searchFilter.off();
	});

	handleTopBarFix();
	handleAutocomplete();
	handleExitBlur();
	bindButtonSearch();
	closeSearchModal();
}

function handleSearchFilter() {
	var $searchFilter = $('.search-filter');
	var $searchFilterWrapper = $searchFilter.find('.search-filter__wrapper');
	var $searchFilterButton = $searchFilter.find('.search-filter__button');

	var turnOnClick = function() {
		$searchFilterButton.off('click');
		$searchFilterButton.on('click', function(e) {
			e.preventDefault();
			$searchFilter.toggleClass('hide');
		})
	}

	var turnOffClick = function() {
		$searchFilterButton.off('click');
	}

	return {
		on: turnOnClick,
		off: turnOffClick
	}
}


function handleTopBarFix() {

	var num = 0;
	var $headerFix = $('.search__top-wrapper');

	$('.search-wrap').on('scroll', function() {
		$('.search-wrap').scrollTop() > 270 ? appendWrapper() : removeWrapper();

		var $scrollOffset = $(window).scrollTop() + $headerFix.height();
		var $i = -1;
		var $categories = joinCategories() || false;

		if(!$categories) return;
			$('[data-categorie]').each(function() {
				if( $scrollOffset >= $(this).offset().top) { $i++; }
			});

			if(num != $i || $i == 0) {
				num = $i;

				if(typeof $categories[$i] !== 'undefined') {
					if(wrapperExistance()){
						$headerFix.find('.search__categorie-title').remove();
						cloneToFix([ $categories[$i].find('.search__categorie-title') ])
					}
				}
			}
	});

	function appendWrapper() {
		if( !wrapperExistance() ) {
			$headerFix.addClass('fixed');
		}
	}

	function removeWrapper() {
		if( wrapperExistance() ) {
			$headerFix.removeClass('fixed');
			$headerFix.find('.search__categorie-title').remove(); // remove o titulo da categoria do header fixo
		}
	}

	// Reúne as 'categorias' encontradas em um array, para poderem se adicionadas no header reduzido
	function joinCategories() {
		var $categories = [];

		if($('[data-categorie]').length < 1) return;
			$('[data-categorie]').each(function() {
				$categories.push($(this));
			})

		return $categories;
	}

	// Verifica se o wrapper para o header reduzido existe
	function wrapperExistance() {
		return $headerFix.hasClass('fixed');
	}

	// Clona os htmls que serão usados no header reduzidos
	function cloneToFix(elements) {
		for( var e in elements ) {
			elements[e].clone().appendTo('.search__top-wrapper');
		}
	}
}

function handleAutocomplete() {
	var $input = $('#search-input');

	//chama o fitro de resutado de busca conforme os eventos
	$input.on('keydown', filterSearch);

	// chama o json e filtra conforme a devem ser mostratadas a sugestões no site
	function filterSearch(evt){
		var q = $(this).val();

		matches = [];
		substrRegex = new RegExp(q, 'i');

		$.ajax({
			url: '/files/search.json',
			dataType: 'json',
			method: 'GET',
			success: function(response) {
				$.each(response, function(i, str) {
					$.map(str.childCategories, function(n, i){
						if (substrRegex.test(n.value)) {
							matches.push( { value: n.value, parent: str.value } );
						}
					})
				});
				if(q.length > 2 ){
					handleResult(matches, $input, q);
				} else {
					_remove();
				}
			}
		})
	}

	//insere as sugetões no html
	var handleResult = function (output, el, q) {
		_remove();
		$.each(output, function(i, n) {
			$('.search__autocomplete').removeClass('hidden');
			$('.search__autocomplete').append('<div class="search__autocomplete-text" onclick="completeInput(\''+n.value+'\');"><span class="search__autocomplete-term"> '+ _item(n.value, q) +'</span> <span class="search__autocomplete-categorie">in <strong>'+ n.parent+'</strong><span></div>');
		})
	}

	window.completeInput = function(val) {
		$input.val(val);
		_remove();
		$('#exit').fadeOut();
	}

	// remove as divs de sugestão de busca
	var _remove = function() {
		if($('.search__autocomplete-text').length > 0) {
			$('.search__autocomplete-text').remove();
			$('.search__autocomplete').addClass('hidden');
		}
	}

	// marca o texto de acorodo com o caracter inputado
	var _item = function (text, input) {
		var html = input.trim() === '' ? text : text.replace(RegExp($.regExpEscape(input.trim()), "gi"), "<mark>$&</mark>");
		return html;
	};

	$.regExpEscape = function (s) {
		return s.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
	};
}

function handleExitBlur() {
	var $input = $('#search-input'),
		$btnExit = $('#exit');

	var $formSize = function() {
		return parseInt($input.css('fontSize'), 10);
	}

	$input.on('keyup', function(e) {
		if( $(this).val().length === 0 && e.type != 'keypress' ) {
			closeInput();
		}
		else {
			$btnExit.css({
				'left': ($(this).val().length+1) * ($formSize() / 2)
			});
			$btnExit.fadeIn();

			if(parseInt($btnExit.css('left').split('px')[0]) >= 645) {
				$('.search__input-wrapper').addClass('search__input-wrapper--fixed-button');
				$btnExit.css('left', '645px');
			}
			else {
				$('.search__input-wrapper').removeClass('search__input-wrapper--fixed-button');

			}
		}
	})

	$('#exit').on('click', function(e) {
		e.preventDefault();
		$input.val('');
		closeInput()
	})

	function closeInput() {
		$('#exit').fadeOut();
		$('#exit').css('left', 0)
	}
}


function bindButtonSearch() {
	var $buttonclick = $('[data-search--click]');

	if($buttonclick.length < 1) return;

	$buttonclick.each(function(e) {
		$(this).on('click', function(e){
			e.preventDefault();

			// Traz o chekbox do filtro marcado quando passado
			// o atributo data-filter="lorem" no botão que abre a busca.
			var filter = $(this).data('filter');
			$('#' + filter).trigger('click');

			openSearchModal();
		})
	});
}

function openSearchModal() {
	$('body').css('overflow', 'hidden').scrollTop(0);
	$('.search').fadeIn();
}

function closeSearchModal() {

	$('.search__close-btn').on('click', function() {
		$('body').css('overflow', 'inherit');
		$('.search').fadeOut();
	});
}

module.exports = {
	init: init
}
