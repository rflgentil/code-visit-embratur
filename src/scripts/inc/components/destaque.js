
/* Mostra mapa da Página Destinos */

var $destaqueComponent = $('[data-component=destaque-destinos]'),
	isMap;


$(document).ready(function(){
	$('#btn-show-map').click(function(){
		$('.destaque__mapaDestinos').slideToggle("slow");

		// Load map.
		$('.destaque__mapaDestinos').append('<div id="map" data-component="map" data-page="destino-home" data-type="map__simple-icon map__simple-icon--small map__simple-icon--green" class="map"></div>').promise().done(function() {
			if(isMap) return false;

			isMap = true;
			App.Modules.map.loadMap();
		});
	});

	$('#btn-hide-map').click(function(){
		$('.destaque__mapaDestinos').slideUp("slow");
	});
});



/* Efeito de slide horizontal do Banner da interna do Destino */

var $destaqueComponent = $('[data-component=destaque-destino]');

window.size_body = $('body').width();

function init() {
	if($destaqueComponent.length >= 1)
	mapOverlay();
}


var resize = function(){
		var btn = $('#bannerDestinoBtn');
		var banner = $('#bannerDestino');
		var percent = banner.width() / window.size_body;
		var size_button = btn.width()/2;
		var size_body = $('body').width();
		var px_percent = (percent * size_body);
		var actual_left = (percent * size_body) - size_button ;

		btn.css('left',actual_left +'px');
		banner.width(px_percent);
		window.size_body = $('body').width();
}

var animation = function(typeButton){
	//armazena o botão na variavel
	var btn = $('#bannerDestinoBtn');

	//define a variavel porcentagem
	var percent = 0.4;
	var size_body = $('body').width();
	var time_animation = 500;

	if(btn.attr('data-status') == 'open'){
		btn.attr('data-status','close');
		percent = 0.9;
	}
	else{
		btn.attr('data-status','open');

		//Mobile
		if(size_body < 768){
			percent = 0;
		}
		else if(size_body > 768 && size_body < 1208){
		//Tablet
			percent = 0.06;
		}
		else{
			percent = 0.22;
		}
	}
	var banner = $('#bannerDestino');
	var size_button = btn.width()/2;

	var px_percent = (percent * size_body);
	var actual_left = (percent * size_body) - size_button ;

	if(size_body < 768 && typeButton){
		actual_left = actual_left + 54;
	}

	btn.animate({
		left: actual_left +'px'
	}, time_animation);

	banner.animate({
		width:px_percent
	}, time_animation);
}


function mapOverlay() {

	$('#bannerDestinoBtn').on('click',function(){
		var typeButton = $(this).hasClass('destaque__btn--destino-close');


		animation(typeButton);

		if($(this).hasClass('destaque__btn--destino-close')){
			$(this).removeClass('destaque__btn--destino-close');
			$(this).addClass('destaque__btn--destino-open');
		}else{
			$(this).removeClass('destaque__btn--destino-open');
			$(this).addClass('destaque__btn--destino-close');
		}
	} )
		$(window).resize(resize);
		resize();
}

module.exports = {
	init: init
}
