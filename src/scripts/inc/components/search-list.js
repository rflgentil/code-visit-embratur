var $listChoose = $('[data-component="search-list"]');

function init() {
	if($listChoose.length >= 1) {
		hadleListView();
		handleCheckboxSelect();
	}
}


function hadleListView() {

	$buttonsChoose = $('.search-list__button');
	$wrapperList = $('.search-list__wrapper');

	$buttonsChoose.on('click', function() {

		if( checkIsActive( $(this) ) ){
			wrapperClose($(this).data('target'));
			$(this).removeClass('active');
		}  else {
			$buttonsChoose.removeClass('active');
			$wrapperList.hide();
			wrapperOpen($(this).data('target'));
			$(this).addClass('active');
		}

	});

	function checkIsActive(el) {
		return el.hasClass('active') ? true : false;
	}

	function wrapperOpen(el) {
		$('#'+el).show();
	}

	function wrapperClose(el) {
		$('#'+el).hide();
	}
}

function handleCheckboxSelect() {
	var $selectAll = $('.search-list__select-all');

	$selectAll.on('click', function(e) {

		var $checkboxes = $(this).parents('.search-list__wrapper').find(':checkbox');
		$(this).toggleClass('checked');

		if($(this).hasClass('checked')) {
		 $checkboxes.prop('checked', true);
		} else {
		 $checkboxes.prop('checked', false);
		}

		e.preventDefault();
	})
}

module.exports = {
	init: init
}
