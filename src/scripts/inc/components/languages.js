var $idiomasComponent = $('[data-component=languages]');

function init() {
	if($idiomasComponent.length >= 1) handleSelectToggle();
}

function handleSelectToggle() {
	var $label = $('.languages__label');

	$label.on('click', function(e){
		e.preventDefault();
		$(this).parent().toggleClass('active');
		$(this).parents('.languages').toggleClass('active');
	})

	function handleItemSelect() {
		var $item = $('.languages__options-item');

		$item.on('click', function() {
			$(this).addClass('active').siblings().removeClass('active')
			$(this).parents('.languages').find('.languages__label').trigger('click');
		});
	}

	handleItemSelect();
}

module.exports = {
	init: init
}
