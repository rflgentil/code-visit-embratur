var $accordeon = $('[data-component=accordeon]');

function init() {
	$('.accordeon__title').click(function(){
		$('.accordeon__content').slideToggle("slow");
	});
}

module.exports = {
	init: init
}
