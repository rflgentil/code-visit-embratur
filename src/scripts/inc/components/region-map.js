var $regionMap = $('[data-component="region-map"]');
var $customCombo = $regionMap.find('[data-component="custom-combo"]');
var $customComboJs = require('./combo-box');

function init(region, place) {
	region = $regionMap.data('region');
	place = 'BR-' + $regionMap.data('place');

	if($regionMap.length >= 1) {
		if(typeof region !== 'string') return;

		handleComboSelect();
		changeRegion(region);
		handleMapChange(new selectMapRegions(region), place);
		insertPin(region, place);
		$customComboJs.insertText($regionMap.find('.custom-combo__options-item'), region)
	}
}

function handleComboSelect() {

	if ($customCombo.lenght < 1) return;

	$customCombo.find('.custom-combo__options-item').on('click', function() {
		var dataRegion = $(this).data('region');
		var region = new selectMapRegions(dataRegion);

		changeRegion(dataRegion)
		handleMapChange(region);

		$('.region-map__initial, .region-map__place').css({
			'display':'none'
		})
	})
}

function insertPin(region, place) {
	var $map = new selectMapRegions(region).Region[0];

	var j = $.grep($map.states, function(n, i) {
		return n.id === place;
	});

	if(j.length < 1) { console.log('State does not exist in region'); return }

		$('#'+place).attr('fill', '#fff83f');
		$('#'+region).prepend('<span class="region-map__map-pin region-map__map-pin--'+place+'"></span>')
}

function changeRegion(region) {
	$('.region-map__map-svg').hide();
	$('#'+region).show();
}

function selectMapRegions(region) {
	var Regions = [
		{
			"region" : "north",
			"map": "",
			"states": [
				{
					"id": "BR-AC",
					"uf": "Ac",
					"name": "Acre"
				},
				{
					"id": "BR-AM",
					"uf": "Am",
					"name": "Amazonas"
				},
				{
					"id": "BR-AP",
					"uf": "Ap",
					"name": "Amapá"
				},
				{
					"id": "BR-PA",
					"uf": "Pa",
					"name": "Pará"
				},
				{
					"id": "BR-RO",
					"uf": "Ro",
					"name": "Rodnônia"
				},
				{
					"id": "BR-RR",
					"uf": "Rr",
					"name": "Roraima"
				},
				{
					"id": "BR-TO",
					"uf": "To",
					"name": "Tocantins"
				}
			]
		},
		{
			"region" : "northeast",
			"map": "",
			"states": [
				{
					"id": "BR-AL",
					"uf": "Al",
					"name": "Alagoas"
				},
				{
					"id": "BR-BA",
					"uf": "Ba",
					"name": "Bahia"
				},
				{
					"id": "BR-CE",
					"uf": "Ce",
					"name": "Ceará"
				},
				{
					"id": "BR-MA",
					"uf": "Ma",
					"name": "Maranhão"
				},
				{
					"id": "BR-PB",
					"uf": "Pb",
					"name": "Paraíba"
				},
				{
					"id": "BR-PE",
					"uf": "Pe",
					"name": "Pernambuco"
				},
				{
					"id": "BR-PI",
					"uf": "Pi",
					"name": "Piauí"
				},
				{
					"id": "BR-RN",
					"uf": "Rn",
					"name": "Rio Grande do Norte"
				},
				{
					"id": "BR-SE",
					"uf": "Se",
					"name": "Sergipe"
				}
			]
		},
		{
			"region" : "west_central",
			"map": "",
			"states": [
				{
					"id": "BR-DF",
					"uf": "Df",
					"name": "Distrito Federal"
				},
				{
					"id": "BR-GO",
					"uf": "Go",
					"name": "Goiás"
				},
				{
					"id": "BR-MS",
					"uf": "Ms",
					"name": "Mato Grosso do Sul"
				},
				{
					"id": "BR-MT",
					"uf": "Mt",
					"name": "Mato Grosso"
				}
			]
		},
		{
			"region" : "south",
			"map": "",
			"states": [
				{
					"id": "BR-PR",
					"uf": "Pr",
					"name": "Paraná"
				},
				{
					"id": "BR-RS",
					"uf": "Rs",
					"name": "Rio Grande do Sul"
				},
				{
					"id": "BR-SC",
					"uf": "Sc",
					"name": "Santa Catarina"
				}
			]
		},
		{
			"region" : "southeast",
			"map": "",
			"states": [
				{
					"id": "BR-RJ",
					"uf": "Rj",
					"name": "Rio de Janeiro"
				},
				{
					"id": "BR-ES",
					"uf": "Es",
					"name": "Espirito Santo"
				},
				{
					"id": "BR-SP",
					"uf": "Sp",
					"name": "São Paulo"
				},
				{
					"id": "BR-MG",
					"uf": "Mg",
					"name": "Minas Gerais"
				}
			]
		}
	]

	selectMapRegions.prototype.Region = $.grep(Regions, function(n, i) {
		return n.region === region;
	});
}

function handleMapChange(map, path) {
	var $map = map.Region[0];
	var $id = $map.region;
	var $name = $('#region-name span');
	var $uf = $('#region-uf');

	if(path) setRegion(path);

	$('#'+$id).find('path').on('click', function(e) {
		e.preventDefault();
		$(this).attr('fill', '#fff83f').siblings().attr('fill', '#88d3df');
		setRegion($(this).attr('id'));
	});

	function setRegion(path) {
		var j = $.grep($map.states, function(n, i) {
			return n.id === path;
		});

		if(j.length < 1) return;
			$name.html(j[0].name);
			$uf.html(j[0].uf);

			$('.region-map__initial, .region-map__place').css({
				'display':'block'
			})
	}
}

module.exports = {
	init: init
}
