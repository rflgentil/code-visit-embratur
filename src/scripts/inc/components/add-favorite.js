var $addFavoriteComponent 	= $('[data-component=add-favorite]'),
	favoritesModel 			= localStorage.getItem('favoritesModel') ? JSON.parse(localStorage.getItem('favoritesModel')) : [];


function init() {
	if($addFavoriteComponent.length >= 1) {
		isFav();
		toggleFav();
	}
}



// Verifica se a página já está no favoritos e modifica o ícone.
function isFav() {
	var pageId 		= $addFavoriteComponent.data('id'),
		totalCat 	= favoritesModel.length,
		isFav;

	if(totalCat <= 0) {
		$addFavoriteComponent.removeClass('add-favorite--active');
		return false;
	}

	for(var i=0; i<totalCat; i+=1) {
		isFav = favoritesModel[i].items.some(function (element, index, array) {
			return element.id === pageId;
		});

		if(isFav) break;
	};

	if(isFav) {
		$addFavoriteComponent.addClass('add-favorite--active');
	}
	else {
		$addFavoriteComponent.removeClass('add-favorite--active');
	}
}


// faz a troca do botão e verifica se chama a função de add ou remove no click do botão.
function toggleFav() {
	$addFavoriteComponent.on('click', function() {
		var $this 		= $(this),
			isActive 	= $this.hasClass('add-favorite--active');

		if(!isActive) {
			addFav($this);
			$addFavoriteComponent.addClass('add-favorite--active');
		}
		else {
			removeFav();
			$addFavoriteComponent.removeClass('add-favorite--active');
		}
	});
}


// Adiciona aos favoritos.
function addFav($this) {
	var catLength = favoritesModel.length,
		fav 	= {},
		items 	= {
			"id": $this.data('id'),
			"title": $this.data('title'),
			"description": $this.data('description')
		}

	fav.category 	= $this.data('category');
	fav.items 		= [items];

	if(catLength <= 0) {
		favoritesModel.push(fav);
	}
	else {
		for(var i=0; i<catLength; i+=1) {
			if(fav.category === favoritesModel[i].category) {
				favoritesModel[i].items.push(items);
				break;
			}
			else {
				if(i+1 === catLength) {
					favoritesModel.push(fav);
					break;
				}
			}
		};
	}

	setModel();
	App.Modules.favorite.loadFavorite();
}


// Remove dos favoritos.
function removeFav(idItem) {
	var pageId = idItem || $addFavoriteComponent.data('id');

	favoritesModel.forEach(function (element, index, array) {
		var arrCat 		= array,
			indexCat 	= index;

		element.items.forEach(function (element, index, array) {
			if(element.id === pageId) array.splice(index, 1);
			if(array.length <= 0) arrCat.splice(indexCat, 1);
		});
	});

	if(idItem) isFav();
	setModel();
	App.Modules.favorite.loadFavorite();
}


// Atualiza localStorage com novo favorito.
function setModel() {
	localStorage.setItem('favoritesModel', JSON.stringify(favoritesModel));
}


// Recupera os dados dos favoritos.
function getModel() {
	return JSON.parse(localStorage.getItem('favoritesModel'));
}



module.exports = {
	init: init,
	addFav: addFav,
	removeFav: removeFav,
	getFav: getModel
}
