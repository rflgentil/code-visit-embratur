var $alert = $('[data-component="alert"]');

function init() {

	handleCloseAlert();
}

function handleCloseAlert() {
	var $btClose = $alert.find('.alert__bt-close');

	$btClose.on('click', function(e) {
		e.preventDefault();

		$(this).parent().addClass('hidden');
	});
}


module.exports = {
	init: init
}
