$customInputDateComponent = $('[data-component="custom-input-date"]');
$customInputDate = $customInputDateComponent = $('.custom-input-date__input');

function init() {
	if($customInputDate.length >= 1) {
		$customInputDate.datepicker({
			dateFormat: 'M-yy'
		});
	}
}

module.exports = {
	init: init
}
