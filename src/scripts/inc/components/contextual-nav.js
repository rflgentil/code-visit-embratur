var $contextualNav = $('[data-component="contextual-nav"]');

function init() {
	ContextualNavigation();
}

function ContextualNavigation() {
	$('.contextual-nav__bt-close').click(function(){
		$('.contextual-nav').fadeOut(200);
	})
}

module.exports = {
	init: init
}
