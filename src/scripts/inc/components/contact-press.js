var $formComponent 	= $('.imprensa__contact-form'),
	fields 			= [
	{
		id: 'contactName',
		message: 'invalid Name!',
		validate: function() {
			return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined && $('#' + this.id).val() != this.message;
		}
	},
	{
		id: 'contactPhone',
		message: 'invalid Phone!',
		validate: function() {
			return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined && $('#' + this.id).val() != this.message;
		}
	},
	{
		id: 'contactEmail',
		message: 'invalid E-mail!',
		validate: function() {
			var cEmail = $('#' + this.id).val();
			var emailFilter = /^.+@.+\..{2,}$/;
			var illegalChars = /[\(\)\<\>\,\;\:\\\/\"\[\]]/
			return ((emailFilter.test(cEmail)) && cEmail.match(illegalChars) == null);
		}
	},
	{
		id: 'contactOrganization',
		message: 'invalid Organization!',
		validate: function() {
			return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined && $('#' + this.id).val() != this.message;
		}
	},
	{
		id: 'contactSubject',
		message: 'invalid Subject!',
		validate: function() {
			return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined && $('#' + this.id).val() != this.message;
		}
	},
	{
		id: 'contactMessage',
		message: 'invalid Message!',
		validate: function() {
			return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined && $('#' + this.id).val() != this.message;
		}
	}
];



function init() {
	if($formComponent.length >= 1) submitFormContact();
}


function submitFormContact() {
	$formComponent.submit(function() {
		var formValid 	= true,
			message 	= '';

		//varre todos os campos no array
		$(fields).each(function(i, el) {
			var $ele = $('#' + el.id);

			$ele.focusin(function() {
				if($ele.val() === el.message) {
					$(this).val('');
					$ele.parent().removeClass('error');
				}
			});

			if (!el.validate()) {
				formValid = false;
				$('[data-component="box-msg-error"]').slideDown(500);

				// $ele.parent().addClass('error').children().val(el.message);
				$ele.parent().addClass('error');
			} else {
				$ele.parent().removeClass('error');
			}
		});

		//Verifica se o form é valido
		if (formValid) {
			$('[data-component="box-msg-error"]').slideUp(500);

			$.ajax({
				url: '/',
				type: 'GET',
				data: {
					name: $('#contactName').val(),
					phone: $('#contactPhone').val(),
					email: $('#contactEmail').val(),
					organization: $('#contactOrganization').val(),
					subject: $('#contactSubject').val(),
					message: $('#contactMessage').val(),
				},
				success: function() {
					$('.imprensa__contact-block--send').fadeOut(800);
					$('.imprensa__contact-block--success').fadeIn(800);
					$('.imprensa__contact-block').addClass('success');
					$('.imprensa__contact-field').children().prop('disabled', true);

					window.setTimeout(function(argument) {
						$('.imprensa__contact-block--send').fadeIn(800);
						$('.imprensa__contact-block--success').fadeOut(800);

						$('.imprensa__contact-block').removeClass('success');
						$('.imprensa__contact-field').children().val('').prop('disabled', false);;
					}, 6000);
				}
			});
		}

		return false;
	});

}

module.exports = {
	init: init
}
