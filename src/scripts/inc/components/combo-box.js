var $customComboComponent = $('[data-component=custom-combo]');


function init() {
	if($customComboComponent.length >= 1) {
		var $handlecCombo = handleCombo();

		$handlecCombo.init();
		handleComboSelect();
	}
}

function handleCombo() {

	function init() {
		$customComboComponent.each(function() {
			$(this).find('.custom-combo__button').bind('click', handle);
		});
	}

	function handle() {
		$(this).parent().hasClass('active') ? closeCombo($(this).parent()) : openCombo($(this).parent())
	}

	var openCombo = function(el) {
		el.addClass('active');
	}

	var closeCombo = function(el) {
		el.removeClass('active');
	}

	return {
		init: init,
		close: closeCombo
	}
}

function handleComboSelect() {
	var $select = $customComboComponent.find('.custom-combo__button');
	var $selectItem = $customComboComponent.find('.custom-combo__options-item');
	var $selectWrapper = $customComboComponent.find('.custom-combo__options');

	var $combo = handleCombo();

	$selectItem.on('click', function(e) {
		$(this).addClass('active').siblings().removeClass('active');

		handleinsertText($(this), $(this).html());
		$combo.close( $(this).parents('.custom-combo') );
	})

	// chama o plugin para scroll customizado
	$customComboComponent.each(function(){
		if( $(this).data('scroll') ) handleCustomScroll($(this));
	})
}

function handleinsertText(el, txt) {
	el.parents('.custom-combo').find('.custom-combo__button').html(txt);
	el.parents('.custom-combo').find('input[type=hidden]').val(txt);
}

function handleCustomScroll(el) {
	var $selectItem = el.find('.custom-combo__options');

	$selectItem.mCustomScrollbar();
}

module.exports = {
	init: init,
	insertText: handleinsertText
}
