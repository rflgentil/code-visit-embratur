var $favoriteComponent 	= $('[data-component=favorite]'),
	favoritesModel;

function init() {
	favorites = App.Modules.addFavorite;

	toggleFavorite();
	loadFavorite();
}



function loadFavorite() {

	if(!favorites.getFav()) return false;

	if(favorites.getFav().length <= 0) {
		//Add mensagem de no-results.
		$('.favorite__no-results').addClass('favorite__no-results--active');
	}
	else {
		//Retira mensagem de no-results.
		$('.favorite__no-results').removeClass('favorite__no-results--active');
	}


	$('.favorite__container-category').remove();

	var totalCat 	= favorites.getFav().length,
		totalItem 	= 0,
		tplFav 		= '',
		tplItems 	= '';


	for(var i=0; i<totalCat; i+=1) {
		totalItem += favorites.getFav()[i].items.length;

		favorites.getFav()[i].items.forEach(function (element, index, array) {
			tplItems += "<li id='" + element.id + "' class='favorite__item-list'>" +
							"<a href='#' class='favorite__item-list-link'>" +
								"<span class='favorite__name'>" + element.title + "</span>" +
								"<p class='favorite__description'>" + element.description + "</p>" +
							"</a>" +
							"<button class='favorite__btn-remove'></button>" +
						"</li>"
		});

		tplFav += "<div class='favorite__container-category'>" +
					"<span class='favorite__category'>" + favorites.getFav()[i].category +"</span>" +
					"<ul class='favorite__list'>" +
						tplItems +
					"</ul>" +
					"<button class='favorite__btn-more-fav favorite__btn-more-fav--show'>Show all " + favorites.getFav()[i].items.length + " attractions</button>" +
					"<button class='favorite__btn-more-fav favorite__btn-more-fav--close'>Close attractions</button>" +
				"</div>";

		tplItems = '';
	}

	$('.favorite__container-fav').prepend(tplFav).promise().done(function() {
		$('.favorite__container-category').each(function() {
			var $this 			= $(this),
				qtdItemCategoty = $this.find('.favorite__item-list').length,
				needPagination 	= qtdItemCategoty > 3;

			pagination($this, needPagination);
		});

		customScroll();
	});

	//Adiciona o numero total de favoritos ao icone em destaque.
	$('.favorite__count').text(totalItem);

	// Evento de click do botão de paginação.
	$favoriteComponent.find('.favorite__btn-more-fav').on('click', handleBtnMorefav);

	// Evente de click do botão de excluir favorito.
	$favoriteComponent.find('.favorite__btn-remove').on('click', handleBtnExclude);
}


// Abre e fecha favoritos.
function toggleFavorite() {
	$favoriteComponent.find('.favorite__button').on('click', function() {
		var $this = $(this);

		$this.parent('[data-component=favorite]').toggleClass('favorite--active');
	});
}


function pagination($this, pagination) {
	if(pagination) {
		$this.addClass('active-pagination');
	}
	else {
		$this.removeClass('active-pagination').find('.favorite__item-list:last-child').addClass('no-border');
	}
}


function handleBtnMorefav(data) {
	var $this = $(this);

	$this.parent('.favorite__container-category').find('.favorite__btn-more-fav').toggle();
	$this.parent('.favorite__container-category').find('.favorite__list').toggleClass('full-list');
}


function handleBtnExclude() {
	var $this 		= $(this),
		$itemList 	= $this.parent('.favorite__item-list'),
		itemId 		= parseInt($this.parents('.favorite__item-list').attr('id'));

	$itemList.addClass('remove-item');
	window.setTimeout(function() {
		$itemList.remove();
		favorites.removeFav(itemId);
	}, 500);
}


// Barra de rolagem customizada.
function customScroll() {
    $(".favorite__wrap-list").mCustomScrollbar({
	    callbacks:{
	        onInit:function(){

	        	// Adiciona elemento para fazer o gradient no final do scroll
	        	// após o plugin terminar de ser carregado.
				$('.favorite__wrap-list').append('<span class="favorite__mask"></span>');
		    }
	    }
	});
}



module.exports = {
	init: init,
	loadFavorite: loadFavorite
}
