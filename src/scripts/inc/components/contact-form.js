var $formComponent = $('[data-component=contact-form]');
var fields = [{
	id: 'cname',
	message: 'invalid name!',
	validate: function() {
		return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined;
	}
}, {
	id: 'cmsg',
	message: 'invalid message!',
	validate: function() {
		return $('#' + this.id).val() != '' && $('#' + this.id).val() != undefined;
	}
}, {
	id: 'cmail',
	message: 'invalid e-mail!',
	validate: function() {
		var cEmail = $('#' + this.id).val();
		var emailFilter = /^.+@.+\..{2,}$/;
		var illegalChars = /[\(\)\<\>\,\;\:\\\/\"\[\]]/
		return ((emailFilter.test(cEmail)) && cEmail.match(illegalChars) == null);
	}
}];

function init() {
	submitFormContact();
}

function submitFormContact() {
	$('#main-form').submit(function() {
		return false;
	});

	$('#btnForm').click(function(e) {
		e.preventDefault();

		var formValid = true;
		var message = '';

		//varre todos os campos no array
		$(fields).each(function(i, el) {
			if (!el.validate()) {
				formValid = false;
				$('#' + el.id).css("border-color", "rgb(255, 12, 0)");
				$('[data-component="box-msg-error"]').slideDown(500);

				// $('.contact-form__fail--' +el.id).text(el.message).show();

			} else {
				$('#' + el.id).css("border", "");
				// $('.contact-form__fail--' +el.id).hide();
			}
		});

		//Verifica se o form é valido
		if (formValid) {
			$('[data-component="box-msg-error"]').slideUp(500);

			$.ajax({
				url: '/',
				type: 'GET',
				data: {
					name: $('#cname').val(),
					email: $('#cemail').val(),
					message: $('#cmsg').val()
				},
				success: function() {
					$('.contact-form__mail').css("border", "");
					$('.contact-form__overlay').fadeIn(800);
					window.setTimeout(function(argument) {
						$('.contact-form__overlay').fadeOut(800);
						$('.contact-form__name').val('');
						$('.contact-form__mail').val('');
						$('.contact-form__msg').val('');
					}, 6000);
					$('.erro').fadeOut();
				}
			});
		}

	});
}

module.exports = {
	init: init
}
