var $cardTooltip 	= $('[data-component=card-tooltip]'),
	dataTooltip 	= {};


function init() {
	if($cardTooltip.length >= 1) startTooltip();
}


function startTooltip() {
	$cardTooltip.on('mouseenter', enterTooltip.debounce(500));
	$cardTooltip.on('mouseleave', leaveTooltip.debounce(500));
}

function enterTooltip() {
	var $this = $(this);

	if($('body').find('.card-tooltip__wrap').length >=1 ) return false;

	dataTooltip.widthEle 		= $this.outerWidth() / 2;
	dataTooltip.link 			= $this.attr('href');
	dataTooltip.image 			= $this.attr('data-image');
	dataTooltip.section 		= $this.attr('data-section');
	dataTooltip.title 			= $this.attr('data-title');
	dataTooltip.text 			= $this.attr('data-text');
	dataTooltip.offSet 			= $this.offset();
	dataTooltip.widthTooltip	= 164 / 2;
	dataTooltip.id				= $this.attr('data-title').replace(/ /g, '-');
	dataTooltip.heightTooltip	= 0;

	var toolTipBody = {
		'html': '<div id="'+ dataTooltip.id + '" class="card-tooltip__wrap">' +
					'<a href="' + dataTooltip.link  + '">' +
						'<figure class="card-tooltip__figure">' +
							'<img src="' + dataTooltip.image + '" alt="Imagem card">' +
						'</figure>' +
						'<span class="card-tooltip__section">' + dataTooltip.section + '</span>' +
						'<span class="card-tooltip__title">' + dataTooltip.title + '</span>' +
						'<p class="card-tooltip__description">' + dataTooltip.text + '</p>' +
						'<button class="card-tooltip__btn"></button>' +
					'</a>' +
				'</div>'
	}

	$('body').append(toolTipBody.html);


	dataTooltip.heightTooltip = $('.card-tooltip__wrap').outerHeight();

	$('body').find('#' + dataTooltip.id).css({
		'top': dataTooltip.offSet.top - dataTooltip.heightTooltip,
		'left': (dataTooltip.offSet.left + dataTooltip.widthEle) - dataTooltip.widthTooltip
	}).stop(true, true)
	.animate({'opacity': 1, 'top': dataTooltip.offSet.top - (dataTooltip.heightTooltip + 13)}, 300);
}

function leaveTooltip() {
	var $this 	= $(this);

	if($('body').find('.card-tooltip__wrap').length <=0 ) return false;

	window.setTimeout(function(){
		$('body').find('#' + dataTooltip.id).stop(true, true)
		.animate({'opacity': 0, 'top': dataTooltip.offSet.top - dataTooltip.heightTooltip}, 300, function(){
			$('#' + dataTooltip.id).remove();
		});
	}, 1000);

}


module.exports = {
	init: init
}
