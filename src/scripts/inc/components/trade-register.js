var $tradeRegister 			= $('.trade-register__form'),
	$tradeRegisterFildset 	= $('.trade-register__fieldset'),
	$tradeRegisterItem 		= $('.trade-register__form-item'),
	$btnCancel 				= $('.trade-register__btn-cancel'),
	$txtPass				= $('#txtPassword'),
	$txtVerifyPass			= $('#txtVerifyPassword'),
	arrValid 				= [],
	arrCombo 				= [],
	isvalid 				= true;

function init() {
	if($tradeRegister.length >= 1) {
		submitFormContact();
		clearFields();
		listCombo();
	}
}



// Valida se e-mail.
function validEmail($this) {
	var cEmail 			= $this.val(),
		emailFilter 	= /^.+@.+\..{2,}$/,
		illegalChars 	= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;

	return ((emailFilter.test(cEmail)) && cEmail.match(illegalChars) == null);
}

// Valida campo vazio.
function validEmpty($this) {
	return $this.val() != '' && $this.val() != undefined;
}

// Valida checkbox/radio.
function validCheckRadio($this) {
	return $this.prop('checked');
}

// Valida password igual.
function validPassword($this) {
	var pass 		= $txtPass.val(),
		verifyPass 	= $this.val();

	return ($this.val() != '' && $this.val() != undefined && verifyPass === pass);
}


// Add classe de erro aos campos.
function addClassError($this) {
	$this.parents('.trade-register__form').addClass('error');
	$this.parents('.trade-register__fieldset').addClass('error');
	$this.parents('.trade-register__form-item').addClass('error');
}


// Varre campos e aplica validação.
function validForm() {
	$tradeRegister.find('.required').each(function(i) {
		var $this 		= $(this);
		 	typeValue 	= $this.attr('type');

		if(typeValue === 'checkbox' || typeValue === 'radio') {
			if(!validCheckRadio($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.trade-register__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
		else if(typeValue === 'email') {
			if(!validEmail($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.trade-register__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
		else if($this.attr('id') === 'txtVerifyPassword') {
			if(!validPassword($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.trade-register__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
		else {
			if(!validEmpty($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.trade-register__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
	});


	// retira classe de error do fieldset.
	$tradeRegisterFildset.each(function() {
		var $this 		= $(this),
			allValid 	= $this.find('.trade-register__form-item.error').length;

		if(allValid <= 0) $this.removeClass('error');
	});

	isvalid = arrValid.some(function(ele) {
        return ele === false;
    });

	// retira classe de error do form.
	if(!isvalid) $tradeRegister.removeClass('error');
}


// Aplica validação na modificação dos campos.
function changeValidation() {
	$('.trade-register__form-item.error .required').on('change', validForm);

	$('.trade-register__form-item.error .custom-combo__options-item').on('click', function() {
		var $this = $(this),
			cbValue = $this.html();

		$this.parents('.custom-combo').find('input[type=hidden]').val(cbValue).trigger('change');
	});
}


// Serialize form data p/ Json
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};



// Submit form.
function submitFormContact() {
	$tradeRegister.submit(function() {
		validForm();
		changeValidation();

		function serializeForm() {
			var o = {};
			var a = this.serializeArray();

			$.each(a, function() {
			if (o[this.name]) {
			   if (!o[this.name].push) {
			       o[this.name] = [o[this.name]];
			   }
			   o[this.name].push(this.value || '');
			} else {
			   o[this.name] = this.value || '';
			}
			});
			return o;
		}

		//Verifica se o form é valido
		if(!isvalid) {
			var formModel = $(this).serializeObject();

			$.ajax({
				url: '/',
				type: 'GET',
				data: formModel,
				success: function() {
					$tradeRegister.addClass('trade-register__form--success').children().prop('disabled', true);
					$('.trade-register__form-item.error .required').off('change');

					window.setTimeout(function(argument) {
						// $('.trade-register__fieldset input, .trade-register__fieldset textarea').val('');
						// $('.trade-register__fieldset input[type=checkbox]').prop('checked', false);
						$btnCancel.trigger('click');
						$tradeRegister.removeClass('trade-register__form--success').children().val('').prop('disabled', false);
					}, 6000);
				}
			});
		}
		else {
			$('html, body').stop(true, true).animate({scrollTop: 0}, 1000);
		}

		return false;
	});

}


// Gera array com objeto de todos os combobox customizados do form
// com ID e texto inicial.
function listCombo() {
	$('.custom-combo').each(function() {
		var $this 	= $(this),
			cbId 	= $this.find('input[type=hidden]').attr('id'),
			cbText	= $this.find('.custom-combo__button').text();

		arrCombo.push({
			id: cbId,
			text: cbText
		})
	});
}


// Limpa campos
function clearFields() {
	$btnCancel.on('click', function() {

		// Preenche os combobox customizados com o texto inicial.
		arrCombo.forEach(function(item) {
			$('#' + item.id).val('').siblings('.custom-combo__button').text(item.text);
		});

		// Limpa campos com erro.
		$tradeRegister.removeClass('error');
		$tradeRegisterFildset.removeClass('error');
		$tradeRegisterItem.removeClass('error');
	});
}



module.exports = {
	init: init
}
