var $breadcrumb = $('[data-component=breadcrumb]');

function init() {
	blockBreadcrumbScroll();
}


// Trava Header no scroll
function blockBreadcrumbScroll() {
	$(window).scroll(function () {
		if($(window).width() < 1208) return false;

		if ($(this).scrollTop() > 56) {
		  $breadcrumb.addClass("breadcrumb--fixed");
		} else {
		  $breadcrumb.removeClass("breadcrumb--fixed");
		}
	});
}

module.exports = {
	init: init
}
