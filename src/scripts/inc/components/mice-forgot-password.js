var $fieldValitation = $('[data-validate--password]');
var formModel = { };

function init() {

	if($fieldValitation.length < 1) return;

	$fieldValitation.on('submit',handleEmailSubmit);

}

function handleEmailSubmit(e) {
	e.preventDefault();

	var $email = $('#email');

	if( validEmail( $email ) ) {

		removeClassError();
		showMessageForm();

		formModel.email = $email;

		// $.ajax({
		// 	url: '/',
		// 	type: 'GET',
		// 	data: formModel,
		// 	success: function() {
		//
		// 	}
		// });
	} else {
		addClassError()
	}
}

function addClassError () {
	$fieldValitation.find('.trade-login__label').addClass('error');
	$fieldValitation.parent().find('.alert-intern').fadeIn();
}

function removeClassError () {
	$fieldValitation.find('.trade-login__label').removeClass('error');
	$fieldValitation.parent().find('.alert-intern').fadeOut();
}

function showMessageForm() {
	$('.trade-login__form').fadeOut().promise().done(function() {
		$('.trade-login__message').fadeIn();
	})
}

// Valida de e-mail é valido.
function validEmail($this) {
	var cEmail 			= $this.val(),
		emailFilter 	= /^.+@.+\..{2,}$/,
		illegalChars 	= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;

	return ((emailFilter.test(cEmail)) && cEmail.match(illegalChars) == null);
}

module.exports = {
	init: init
}
