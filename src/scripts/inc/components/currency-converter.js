var $currencyConverter 	= $('[data-component=currency-converter]'),
	$convertFrom 		= $("#cbConvertFrom"),
	$convertTo 			= $("#cbConvertTo"),
	$inicialValue 		= $("#inicial-value"),
	$temporaryValue 	= $("#temporary-value"),
	$resultConvertion 	= $("#result-convertion"),
	$changeCurrence 	= $('.currency-converter__refresh'),
	$resultSymbol 		= $('.currency-converter__result-symbol'),
	$startSymbol 		= $('.currency-converter__start-symbol'),
	currency 			= {};


function init() {
	if($currencyConverter.length >= 1) {
		initialValue();
		masks();
		requestCurrency();
		triggers();
		changeCurrency();
	}
}



// Request das infos. de cada moeda.
function requestCurrency(url, cb) {

	$.get('https://api.fixer.io/latest?base=BRL&symbols=USD,EUR', function(data) {
		currency.brl = data.rates;
		converter();
	});

	$.get('https://api.fixer.io/latest?base=USD&symbols=BRL,EUR', function(data) {
		currency.usd = data.rates;
	});

	$.get('https://api.fixer.io/latest?base=EUR&symbols=BRL,USD', function(data) {
		currency.eur = data.rates;
	});
}


// Valores inicias no carregamento.
function initialValue() {
	$('.currency-converter__from').find('.custom-combo__options-item').eq(0).trigger('click');
	$('.currency-converter__to').find('.custom-combo__options-item').eq(2).trigger('click');
	isBrl();
}


function triggers() {
	$currencyConverter.find('.custom-combo__options-item').on("click", function() {
		converter();
	});

	$inicialValue.on("keyup", function() {
		converter();
	});
};


// Aplica conversão de a cordo com a moeda selecionada.
function converter() {
	var baseCurrency 	= $convertTo.val().split('-')[0].trim(),
		startCurrency 	= $convertFrom .val().split('-')[0].trim(),
		startSymbol 	= startCurrency === 'USD' ? '$' : startCurrency === 'EUR' ? '€' : "R$",
		resultSymbol 	= baseCurrency === 'USD' ? '$' : baseCurrency === 'EUR' ? '€' : "R$",
		urlRequest 		= '',
		value;


	$temporaryValue.val($inicialValue.cleanVal());
	$temporaryValue.unmask().mask('#0##.00', {reverse: true});


	// Verifica qual é a moeda de conversão final.
	if(baseCurrency === 'USD') {
		value = (startCurrency === "BRL") ? ($temporaryValue.val() / currency.usd.BRL) : ($temporaryValue.val() / currency.usd.EUR);
		applyChangeCurrency(startSymbol, resultSymbol, value);
	}
	else if(baseCurrency === 'EUR') {
		value = (startCurrency === "BRL") ? ($temporaryValue.val() / currency.eur.BRL) : ($temporaryValue.val() / currency.eur.USD);
		applyChangeCurrency(startSymbol, resultSymbol, value);
	}
	else {
		value = (startCurrency === "USD") ? ($temporaryValue.val() / currency.brl.USD) : ($temporaryValue.val() / currency.brl.EUR);
		applyChangeCurrency(startSymbol, resultSymbol, value);
	}

};


// Quando troca de moeda, faz a troca do simbolo e o cálculo da conversão.
function applyChangeCurrency(startSymbol, resultSymbol, value) {
	var conversion 	= (value) ? value.toFixed(2).replace(/\./g, '') : "";

	$startSymbol.text(startSymbol);
	$resultSymbol.text(resultSymbol);
	$resultConvertion.val(conversion);
	masks();
}


// Alica mask nos campos.
function masks() {
	var patternFrom = $convertFrom.val().split('-')[0].trim() === 'EUR' ? '#,##0.00' : '#.##0,00';
		patternTo = $convertTo.val().split('-')[0].trim() === 'EUR' ? '#,##0.00' : '#.##0,00';

	$inicialValue.mask(patternFrom, {reverse: true});
	$resultConvertion.unmask().mask(patternTo, {reverse: true});
};


// Verifica se é a moeda é o Real e bloqueia o campo.
function isBrl() {
	$('.currency-converter .custom-combo').each(function() {
		var $this = $(this),
			isBrl = $this.find('input[type=hidden]').val();

		if(isBrl === 'BRL - Brazilian real') {
			$this.addClass('custom-combo--locked');
		}
		else {
			$this.removeClass('custom-combo--locked');
		}
	});
}


// Inverte as moedas.
function changeCurrency() {
	$changeCurrence.on('click', function(e) {
		e.preventDefault();

		var initialValueFrom 	= $convertFrom.val(),
			initialValueTo 		= $convertTo.val();

		$('.currency-converter__from').find('.custom-combo__options-item').each(function() {
			var $this = $(this);

			if($this.text() === initialValueTo) $this.trigger('click');
		});

		$('.currency-converter__to').find('.custom-combo__options-item').each(function() {
			var $this = $(this);

			if($this.text() === initialValueFrom) $this.trigger('click');
		});

		isBrl();

		// var valueCurrency = ($convertFrom.val().split('-')[0].trim() === "EUR") ? '€' : '$';

		// if($resultSymbol.text() === 'R$') {
		// 	$resultSymbol.text(valueCurrency);
		// 	$startSymbol.text('R$');
		// }
		// else{
		// 	$resultSymbol.text('R$');
		// 	$startSymbol.text(valueCurrency);
		// }

	});
}



module.exports = {
	init: init
}
