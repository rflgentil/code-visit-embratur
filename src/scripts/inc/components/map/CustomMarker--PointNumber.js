"use strict";

function CustomMarker(point, map, args) {
	this.latlng = point;
	this.args = args;
	this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	var self = this;
	var div = this.div;

	if(!div) {
		div = this.div = createDIV(self.args.number, self.args.verdePadrao);

		if(typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}

		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(self, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if(point) {
		div.style.left = point.x + 'px';
		div.style.top = point.y + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if(this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;
}

function createDIV(number, verdePadrao) {
	var corVerde = verdePadrao ? '#7dc243' : '#038e4d';
	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.style.cursor = 'pointer';
	div.style.width = '34px';
	div.style.height = '34px';
	div.style.marginLeft = '-17px';
	div.style.marginTop = '-17px';


	var divCirculoExterno = document.createElement('div');
	divCirculoExterno.style.width='100%';
	divCirculoExterno.style.height='100%';
	divCirculoExterno.style.backgroundColor=corVerde;
	divCirculoExterno.style.position='absolute';
	divCirculoExterno.style.left='0';
	divCirculoExterno.style.top='0';
	divCirculoExterno.style.zIndex='0';
	divCirculoExterno.style.borderRadius='100px';
	divCirculoExterno.style.opacity='0.5';


	var divCirculoInterno = document.createElement('div');
	divCirculoInterno.style.position='absolute';
	divCirculoInterno.style.left='5px';
	divCirculoInterno.style.top='5px';
	divCirculoInterno.style.width='24px';
	divCirculoInterno.style.height='24px';
	divCirculoInterno.style.border='2px solid #fff';
	divCirculoInterno.style.backgroundColor=corVerde;
	divCirculoInterno.style.color='#fff';
	divCirculoInterno.style.fontSize='11px';
	divCirculoInterno.style.fontFamily='sans-serif';
	divCirculoInterno.style.fontWeight='bold';
	divCirculoInterno.style.textAlign='center';
	divCirculoInterno.style.lineHeight='21px';
	divCirculoInterno.style.zIndex='1';
	divCirculoInterno.style.borderRadius='100px';
	divCirculoInterno.innerHTML=number;

	div.appendChild(divCirculoExterno);
	div.appendChild(divCirculoInterno);

	return div;
}


module.exports = CustomMarker;
