"use strict";

function CustomMarkerPointLabel(point, map, args) {
	this.latlng = point;
	this.args = args;
	this.setMap(map);
}

CustomMarkerPointLabel.prototype = new google.maps.OverlayView();

CustomMarkerPointLabel.prototype.draw = function() {
	var self = this;
	var div = this.div;

	if(!div) {
		div = this.div = createDIV(self.args.label, self.args.bold);

		if(typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}

		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(self, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if(point) {
		div.style.left = point.x + 'px';
		div.style.top = point.y + 'px';
	}
};

CustomMarkerPointLabel.prototype.remove = function() {
	if(this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}
};

CustomMarkerPointLabel.prototype.getPosition = function() {
	return this.latlng;
}

function createDIV(label, bold) {
	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.style.cursor = 'pointer';


	var divSeta = document.createElement('div');
	divSeta.style.width='14px';
	divSeta.style.height='9px';
	divSeta.style.backgroundImage='url(/images/map--label-seta.png)';
	divSeta.style.position='absolute';
	divSeta.style.left='0';
	divSeta.style.top='0';
	divSeta.style.zIndex='0';


	var divLabel = document.createElement('div');
	divLabel.style.position='relative';
	divLabel.style.zIndex='1';
	divLabel.style.lineHeight = '24px';
	divLabel.style.backgroundColor = 'rgba(123, 185, 35, 1)';
	divLabel.style.padding = '0 11px';
	divLabel.style.color = '#fff';
	if(bold) {
		divLabel.style.fontWeight = 'bold';
		divLabel.style.fontSize = '15px';
	} else {
		divLabel.style.fontWeight = 'normal';
		divLabel.style.fontSize = '13px';
	}

	divLabel.style.borderRadius = '100px';
	divLabel.style.marginLeft = '9px';
	divLabel.style.marginTop = '2px';
	divLabel.innerHTML = label;

	div.appendChild(divSeta);
	div.appendChild(divLabel);

	return div;
}


module.exports = CustomMarkerPointLabel;
