"use strict";

function CustomMarker(pointA, pointB, map, args) {
	var pointStart = {lat: pointA.lat(), lng: pointA.lng()};
	this.anchorPoints = {x: 'A', y: 'A'};

	console.log(pointA.lat(), pointA.lng(), pointB.lat(), pointB.lng());

	if(pointB.lat() > pointA.lat()) {
		pointStart.lat = pointB.lat();
		this.anchorPoints.y = 'B';
	}

	if(pointB.lng() < pointA.lng()) {
		pointStart.lng = pointB.lng();
		this.anchorPoints.x = 'B';
	}

	pointStart = new google.maps.LatLng(pointStart.lat, pointStart.lng);

	this.latlng = pointStart;
	this.points = [pointA, pointB];
	this.args = args;
	this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	var self = this;
	var div = this.div;

	var divPointA = self.getProjection().fromLatLngToDivPixel(self.points[0]);
	var divPointB = self.getProjection().fromLatLngToDivPixel(self.points[1]);
	var width = Math.abs(parseInt(divPointB.x-divPointA.x));
	var height = Math.abs(parseInt(divPointB.y-divPointA.y));

	if(!div) {
		div = this.div = createDIV(width, height, this.args.heading, this.args.tempoDeVoo);

		if(typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}

		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(self, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	} else {
		div.style.width = width + 'px';
		div.style.height = height + 'px';
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if(point) {
		div.style.left = point.x + 'px';
		div.style.top = point.y + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if(this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;
}

function createDIV(width, height, heading, tempoDeVoo) {
	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.style.cursor = 'pointer';
	div.style.width = width + 'px';
	div.style.height = height + 'px';
	div.style.lineHeight = '20px';
	div.style.backgroundColor = 'transparent';
	div.style.padding = '0 12px';
	div.style.color = '#000';

	var divPlane = document.createElement('div');
	divPlane.style.width = '36px';
	divPlane.style.height = '36px';
	divPlane.style.border = '1px solid #7fc143';
	divPlane.style.borderRadius = '1000px';
	divPlane.style.backgroundColor = '#fff';
	divPlane.style.backgroundSize = '22px 23px';
	divPlane.style.backgroundPosition = '50% 50%';
	divPlane.style.backgroundRepeat = 'no-repeat';
	divPlane.style.backgroundImage = 'url(/images/map-aviao.svg)';
	divPlane.style.position = 'absolute';
	divPlane.style.transformOrigin = '50% 50%';
	divPlane.style.transform = 'rotate(' + (heading+180) + 'deg)';
	divPlane.style.top = '50%';
	divPlane.style.left = '50%';
	divPlane.style.marginTop = '-18px';
	divPlane.style.marginLeft = '-18px';
	divPlane.style.zIndex = '1';

	var divFlightETA = document.createElement('div');
	divFlightETA.style.height = '20px';
	divFlightETA.style.lineHeight = '20px';
	divFlightETA.style.backgroundColor = '#fff';
	divFlightETA.style.position = 'absolute';
	divFlightETA.style.top = '50%';
	divFlightETA.style.marginTop = '-10px';
	divFlightETA.style.right = '50%';
	divFlightETA.style.borderRadius = '100px';
	divFlightETA.style.zIndex = '0';
	divFlightETA.style.padding = '0 26px 0 12px';
	divFlightETA.style.whiteSpace = 'nowrap';

	divFlightETA.innerHTML = tempoDeVoo + " hours";


	div.appendChild(divPlane);
	div.appendChild(divFlightETA);


	// div.style.borderRadius = '100px';
	// div.innerHTML = parseInt(Math.random()*100) + " hours";

	return div;
}


module.exports = CustomMarker;
