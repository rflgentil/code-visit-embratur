var $miceContact 	= $('.mice-contact__form'),
	itemPosition 	= $('.mice-contact__title').offset(),
	txtInitialCombo = $miceContact.find('.custom-combo__button').text();
	arrValid 		= [],
	isvalid 		= true,
	formModel 		= {};

function init() {
	if($miceContact.length >= 1) {
		submitFormContact();
		closeMsgError();
	}
}



// Valida de e-mail é valido.
function validEmail($this) {
	var cEmail 			= $this.val(),
		emailFilter 	= /^.+@.+\..{2,}$/,
		illegalChars 	= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;

	return ((emailFilter.test(cEmail)) && cEmail.match(illegalChars) == null);
}

// Valida campo vazio.
function validEmpty($this) {
	return $this.val() != '' && $this.val() != undefined;
}

// Valida checkbox/radio.
function validCheckRadio($this) {
	return $this.prop('checked');
}


// Add classe de erro aos campos.
function addClassError($this) {
	$this.parents('.mice-contact__form').addClass('error');
	$this.parents('.mice-contact__fieldset').addClass('error');
	$this.parents('.mice-contact__form-item').addClass('error');
}


// Varre campos e aplica validação.
function validForm() {
	$miceContact.find('.required').each(function(i) {
		var $this 		= $(this);
		 	typeValue 	= $this.attr('type');

		if(typeValue === 'checkbox' || typeValue === 'radio') {
			if(!validCheckRadio($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.mice-contact__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
		else if(typeValue === 'email') {
			if(!validEmail($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.mice-contact__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
		else {
			if(!validEmpty($this)) {
				addClassError($this);
				arrValid[i] = false;
			}
			else {
				$this.parents('.mice-contact__form-item').removeClass('error');
				arrValid[i] = true;
			}
		}
	});

	$('.mice-contact__fieldset').each(function() {
		var $this 		= $(this),
			allValid 	= $this.find('.mice-contact__form-item.error').length;

		if(allValid <= 0) $this.removeClass('error');
	});

	isvalid = arrValid.some(function(ele) {
        return ele === false;
    });

	if(!isvalid) $('.mice-contact__form').removeClass('error');
}


// Aplica validação na modificação dos campos.
function changeValidation() {
	$('.mice-contact__form-item.error .required').on('change', validForm);

	$('.mice-contact__form-item.error .custom-combo__options-item').on('click', function() {
		var $this = $(this),
			cbValue = $this.html();

		$this.parents('.custom-combo').find('input[type=hidden]').val(cbValue).trigger('change');
	});
}


// Submit form.
function submitFormContact() {
	$miceContact.submit(function() {

		validForm();
		changeValidation();

		//Verifica se o form é valido
		if(!isvalid) {

			formModel.name = $('#txtName').val();
			formModel.email = $('#txtEmail').val();
			formModel.eventTitle = $('#txtEventTitle').val();
			formModel.people = $('#cbPeople').val();
			formModel.eventDescription = $('#txtEventDescription').val();
			formModel.news = $('#checkNews').prop('checked');

			$.ajax({
				url: '/',
				type: 'GET',
				data: formModel,
				success: function() {
					$('.mice-contact__form').addClass('mice-contact__form--success').children().prop('disabled', true);
					$('.mice-contact__form-item.error .required').off('change');

					window.setTimeout(function(argument) {
						$('.mice-contact__fieldset input, .mice-contact__fieldset textarea').val('');
						$('.mice-contact__fieldset input[type=checkbox]').prop('checked', false);
						$miceContact.find('.custom-combo__button').text(txtInitialCombo);
						$('.mice-contact__form').removeClass('mice-contact__form--success').children().val('').prop('disabled', false);
					}, 6000);
				}
			});
		}
		else {
			$('html, body').stop(true, true).animate({scrollTop: itemPosition.top - 150}, 1000);
		}

		return false;
	});

}


function closeMsgError() {
	$('.mice-contact__btn-close-error').on('click', function(e) {
		e.preventDefault();

		$(this).parents('.mice-contact__box-msg-error').slideUp(500);
	});
}

module.exports = {
	init: init
}
