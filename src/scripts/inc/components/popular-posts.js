var $popularPostsComponent 	= $('[data-component=popular-posts]'),
	$itemNav 				= $('.popular-posts__item-description'),
	$boxTitle 				= $('.popular-posts__box-title'),
	$wrapTitle 				= $('.popular-posts__wrap-title'),
	indexItem 				= 0,
	totalItem 				= $itemNav.length,
	timeSlider,
	activeItem;


var $breakpoints = require('../helpers/breakpoint');




function init() {
	if($popularPostsComponent.length >= 1) {

		// No carregamento da página.
		$breakpoints.initialWindow(768, function() {
			sliderMobile();
		}, function(){
			initialItem();
			changeItemAuto();
			startEvents();
		});

		// No resize do browser.
		$breakpoints.resizeWindow(768, function() {
			$boxTitle.off('mouseenter');
			$boxTitle.off('mouseleave');
			$itemNav.off('mouseenter');
			$itemNav.off('mouseleave');
			stopSlider();

			sliderMobile();
		}, function(){
			$wrapTitle.slick('unslick');

			initialItem();
			changeItemAuto();
			startEvents();
		});

	}
}



function initialItem() {
	$itemNav.eq(indexItem).addClass('active');
	$boxTitle.eq(indexItem).addClass('active');
}


function startEvents() {
	$boxTitle.on('mouseenter', stopSlider);
	$boxTitle.on('mouseleave', changeItemAuto);
	$itemNav.on('mouseenter', changeItemHover);
	$itemNav.on('mouseleave', changeItemAuto);
}

// Para a troca automática dos slides.
function stopSlider() {
	clearInterval(timeSlider);
}


// Troca Item.
function changeItem(activeItem) {
	activeItem = $('.popular-posts__item-description.active').index();

	activeItem = activeItem >= totalItem - 1 ? 0 : activeItem + 1;

	$itemNav.eq(activeItem).addClass('active');
	$itemNav.eq(activeItem - 1).removeClass('active');

	$boxTitle.eq(activeItem).addClass('active');
	$boxTitle.eq(activeItem - 1).removeClass('active');
}


// Troca o conteúdo dos Itens de 5 em 5 segundos.
function changeItemAuto() {
	timeSlider = window.setInterval(changeItem, 5000);
}


// Troca Item no hover.
function changeItemHover() {
	var $this 		= $(this),
		indexThis 	= $this.index() -1;

	stopSlider();

	$itemNav.removeClass('active');
	$boxTitle.removeClass('active');
	$this.prev().addClass('active');
	$boxTitle.eq(indexThis).addClass('active');

	changeItem();
}


function sliderMobile() {
	$wrapTitle.slick({
		variableWidth: true,
		centerMode: true,
		dots: false,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
	});
}



module.exports = {
	init: init
}
