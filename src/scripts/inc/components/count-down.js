var $countDown = $('[data-component=count-down]');

function init() {
	if($countDown.length >= 1) countDownChar();
}

function countDownChar() {
	var maxLength = 350;
	
	$('textarea').keyup(function() {
  		var length = $(this).val().length;
  		var length = maxLength-length;
  		$('#chars').text(length);
	});
}



module.exports = {
	init: init
}
