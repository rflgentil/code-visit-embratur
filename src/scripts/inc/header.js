var $header = $('#header');

function init() {
	blockHeaderScroll();
}


// Trava Header no scroll
function blockHeaderScroll() {
	$(window).scroll(function () {
		if($(window).width() < 1208) return false;

		if ($(this).scrollTop() > 56) {
		  $header.addClass("header--fixed");
		} else {
		  $header.removeClass("header--fixed");
		}
	});
}

module.exports = {
	init: init
}
