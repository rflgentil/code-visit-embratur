module.exports = {
	url: {
		map: {
			"exemplo": "/files/mocks/exemplo-componente.json",
			"attractions": "/files/mocks/map-attractions.json",
			"destino-home": "/files/mocks/map-destino-home.json",
			"destino": "/files/mocks/map-destino.json",
			"atividade": "/files/mocks/map-atividade.json",
			"atividade-brasil": "/files/mocks/map-atividade-brasil.json",
			"regiao": "/files/mocks/map-regiao.json",
			"eventos": "/files/mocks/map-eventos.json",
			"experiencia": "/files/mocks/map-experiencia.json"
		}
	}
}
